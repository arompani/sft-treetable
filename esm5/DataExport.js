import { getDataExportFormatter, setDataExportFormatter } from './DataExportFormatFunctions';
export function prepareDataExportTable(table) {
    table.columns = table.columns.map(function (col) { return setDataExportFormatter(col); });
    var baseData = table.data;
    table.data = table.data.map(function (row) {
        return {
            data: row.data.map(function (cell, colIndex) {
                var formatFunction = table.columns[colIndex].formatFunction;
                if (table.columns[colIndex].hideIfNull && (cell === '' || cell === '0' || cell === 0)) {
                    return '';
                }
                return formatFunction ? formatFunction(cell) : cell;
            }),
            options: row.options,
        };
    });
    if (table.totalDefinitions && table.data.length > 0) {
        var totals = table.totalDefinitions.map(function (td) {
            var row = new Array(table.columns.length);
            row.fill('');
            td.columns
                .map(function (ci) {
                return getDataExportFormatter(ci.format)(baseData.map(function (crow) { return crow.data[ci.index]; }).reduce(ci.reduceFunction));
            })
                .map(function (val, index) {
                return { column: td.columns[index], total: val };
            })
                .forEach(function (col) {
                row[col.column.index] = col.total;
            });
            td.textInsertFunctions.forEach(function (tif) {
                row = tif(row);
            });
            return row;
        });
        table.totals = totals;
    }
    return table;
}
export function prepareDataExportStructure(structure) {
    return structure.map(function (str) {
        if (str.type === 'table') {
            return prepareDataExportTable(str);
        }
        else {
            return str;
        }
    });
}
//# sourceMappingURL=DataExport.js.map