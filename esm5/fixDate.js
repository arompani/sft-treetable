export function fixDate(date, nullValue) {
    if (date === undefined || date === null) {
        return nullValue;
    }
    if (Buffer.isBuffer(date)) {
        try {
            var dateString = date.toString('utf8');
            return fixDate(dateString);
        }
        catch (error) {
            // todo
        }
    }
    if (typeof date === 'string') {
        if (date === '' || date === '0000-00-00') {
            return nullValue;
        }
        else if (date.indexOf('.') !== -1 && date.indexOf('Z') === -1) {
            // formato date di sqlite 2018-07-06 02:00:00.000
            var parts = date.split('.')[0].split(' ');
            var sDate = parts[0], sTime = parts[1];
            var dateParts = sDate.split('-').map(function (p) { return Number(p); });
            var timeParts = sTime.split(':').map(function (p) { return Number(p); });
            // tslint:disable-next-line:no-magic-numbers
            return new Date(dateParts[0], dateParts[1] - 1, dateParts[2], timeParts[0], timeParts[1], timeParts[2]);
        }
        else {
            if (date.indexOf('.') !== -1 && date.indexOf('Z') !== -1) {
                return new Date(date);
            }
            else {
                return new Date(Number(date));
            }
        }
    }
    else {
        return new Date(Number(date));
    }
}
//# sourceMappingURL=fixDate.js.map