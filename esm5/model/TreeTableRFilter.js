/**
 * Definizione di una riga
 *
 * @export
 * @class TreeTableRFilter
 * @template T
 */
var TreeTableRFilter = /** @class */ (function () {
    function TreeTableRFilter(options, data) {
        this.options = options;
        this.currentSubRows = this.options.subGenerator ? this.options.subGenerator.getFilters(data, this) : [];
        this.childrenHidden = this.options.hideChildren;
    }
    Object.defineProperty(TreeTableRFilter.prototype, "text", {
        get: function () {
            return this.options.text;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(TreeTableRFilter.prototype, "subRows", {
        get: function () {
            return this.currentSubRows.filter(function (f) { return f.options.forceShow; }).concat((!this.hideChildren ? this.currentSubRows.filter(function (f) { return !f.options.forceShow; }) : []));
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(TreeTableRFilter.prototype, "parent", {
        get: function () {
            return this.options.parent;
        },
        set: function (newParent) {
            this.options.parent = newParent;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(TreeTableRFilter.prototype, "hideChildren", {
        get: function () {
            return this.childrenHidden;
        },
        set: function (newValue) {
            this.childrenHidden = newValue;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(TreeTableRFilter.prototype, "hasSubRows", {
        /**
         * true se il filtro ha sottocolonne (senza contare quelle dovute al collapsed generator)
         *
         * @readonly
         * @type {boolean}
         * @memberof TreeTableRFilter
         */
        get: function () {
            return this.currentSubRows.length > 0;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(TreeTableRFilter.prototype, "isLeaf", {
        get: function () {
            return this.subRows && this.subRows.length === 0;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(TreeTableRFilter.prototype, "isTotalRow", {
        get: function () {
            return this.options.isTotalRow;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(TreeTableRFilter.prototype, "filterFunction", {
        get: function () {
            return this.options.filterFunction;
        },
        enumerable: true,
        configurable: true
    });
    TreeTableRFilter.prototype.getLeaves = function () {
        var activeFilters = this.subRows;
        var leaves = [];
        if (this.currentSubRows.length === 0 || activeFilters.length === 0) {
            leaves.push(this);
        }
        else {
            activeFilters.forEach(function (af) {
                af.getLeaves().forEach(function (l) {
                    leaves.push(l);
                });
            });
        }
        return leaves;
    };
    TreeTableRFilter.prototype.getParsedData = function (data) {
        return this.applyTopFilters(data).filter(this.options.filterFunction);
    };
    TreeTableRFilter.prototype.applyTopFilters = function (data) {
        var filters = [this];
        var currentNode = this;
        var filteredData = data;
        while (currentNode.parent !== undefined) {
            currentNode = currentNode.parent;
            filters = [currentNode].concat(filters);
        }
        // tslint:disable-next-line:prefer-for-of
        for (var i = 0; i < filters.length; i++) {
            filteredData = filteredData.filter(filters[i].options.filterFunction);
        }
        return filteredData;
    };
    TreeTableRFilter.prototype.calculateAttributes = function (currentLevel) {
        this.level = currentLevel;
        var activeFilters = this.subRows;
        var subLeaves = 0;
        var maxSubLevel = currentLevel;
        if (this.subRows.length === 0 || activeFilters.length === 0) {
            this.subLeaves = 0;
            subLeaves = 1;
        }
        else {
            this.subRows.forEach(function (sc) {
                var result = sc.calculateAttributes(currentLevel + 1);
                maxSubLevel = maxSubLevel < result.maxSubLevel ? result.maxSubLevel : maxSubLevel;
                subLeaves += result.subLeaves;
            });
            this.subLeaves = subLeaves;
        }
        this.maxSubLevel = maxSubLevel;
        return { subLeaves: subLeaves, maxSubLevel: maxSubLevel };
    };
    TreeTableRFilter.prototype.propagateTopDownAttributes = function (branchSubLeaves, maxLevel) {
        var activeFilters = this.subRows;
        this.maxLevel = maxLevel;
        this.branchSubLeaves = branchSubLeaves;
        if (!(this.subRows.length === 0 || activeFilters.length === 0)) {
            this.subRows.forEach(function (sc) {
                sc.propagateTopDownAttributes(branchSubLeaves, maxLevel);
            });
        }
    };
    return TreeTableRFilter;
}());
export { TreeTableRFilter };
//# sourceMappingURL=TreeTableRFilter.js.map