var ConstantTreeTableCFilterGenerator = /** @class */ (function () {
    function ConstantTreeTableCFilterGenerator(filters) {
        this.filters = filters;
        this.isDynamic = false;
    }
    ConstantTreeTableCFilterGenerator.prototype.getFilters = function (data, parent) {
        var newFilters = this.filters.map(function (f) { return f.clone(data, parent); });
        newFilters.forEach(function (f) {
            f.parent = parent;
        }); // update parents on filters
        return newFilters;
    };
    ConstantTreeTableCFilterGenerator.prototype.clone = function (data, parent) {
        return new ConstantTreeTableCFilterGenerator(this.filters.map(function (f) { return f.clone(data, parent); }));
    };
    return ConstantTreeTableCFilterGenerator;
}());
export { ConstantTreeTableCFilterGenerator };
//# sourceMappingURL=TreeTableCFilterGenerator.js.map