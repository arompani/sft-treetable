import { BehaviorSubject } from 'rxjs';
/**
 * Gestione delle righe e colonne della tabella e filtraggio
 *
 * @export
 * @class TreeTable
 * @template T
 */
var TreeTable = /** @class */ (function () {
    function TreeTable(rowsFilterGenerator, columnsFilterGenerator, dataTransformer, reduceFunction) {
        this.rowsFilterGenerator = rowsFilterGenerator;
        this.columnsFilterGenerator = columnsFilterGenerator;
        this.dataTransformer = dataTransformer;
        this.reduceFunction = reduceFunction;
        this.rowAttributes = undefined;
        this.columnAttributes = undefined;
        this.columns = [];
        this._columnsLeaves = [];
        this.rows = [];
        this._rowsLeaves = [];
        this.data = [];
        this.fData = [];
        this.filterGeneratorsChanged = false;
        this.rowsFilteredData = [];
        this.fData$ = new BehaviorSubject([]);
    }
    Object.defineProperty(TreeTable.prototype, "rowFiltersGenerator", {
        /**
         * cambia gli attuali generatori di filtri delle righe
         *
         * @memberof TreeTable
         */
        set: function (generator) {
            this.rowsFilterGenerator = generator;
            this.filterGeneratorsChanged = true;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(TreeTable.prototype, "columnFiltersGenerator", {
        /**
         * cambia gli attuali generatori di filtri delle colonne
         *
         * @memberof TreeTable
         */
        set: function (generator) {
            this.columnsFilterGenerator = generator;
            this.filterGeneratorsChanged = true;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(TreeTable.prototype, "currentReduceFunction", {
        /**
         * cambia la funzione di riduzione applicata
         *
         * @memberof TreeTable
         */
        set: function (reduceFunction) {
            this.reduceFunction = reduceFunction;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(TreeTable.prototype, "filteredData$", {
        get: function () {
            if (this.fData === undefined) {
                this.updateData();
            }
            return this.fData$; // apply filters
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(TreeTable.prototype, "filteredData", {
        get: function () {
            if (this.fData === undefined) {
                this.updateData();
            }
            return this.fData;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(TreeTable.prototype, "columnsTree", {
        /**
         * fornisce l'albero completo delle colonne
         *
         * @readonly
         * @type {TreeTableCFilter<T>}
         * @memberof TreeTable
         */
        get: function () {
            return this.columns;
        },
        /**
         * cambia l'albero delle colonne con quello indicato ed avvia la rigenerazione della
         * tabella dei dati filtrati
         *
         * @memberof TreeTable
         */
        set: function (newTree) {
            this.columns = newTree;
            this.updateData();
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(TreeTable.prototype, "columnsLeaves", {
        get: function () {
            return this._columnsLeaves;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(TreeTable.prototype, "rowsTree", {
        /**
         * fornisce l'albero completo delle righe
         *
         * @readonly
         * @type {TreeTableRFilter<T>}
         * @memberof TreeTable
         */
        get: function () {
            return this.rows;
        },
        set: function (newTree) {
            this.rows = newTree;
            this.updateData();
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(TreeTable.prototype, "rowsLeaves", {
        get: function () {
            return this._rowsLeaves;
        },
        enumerable: true,
        configurable: true
    });
    TreeTable.prototype.updateDataAfterColumnsChange = function () {
        var _this = this;
        var _a;
        this.fData = [];
        this._columnsLeaves = (_a = []).concat.apply(_a, this.columns.map(function (c) { return c.getLeaves(); }));
        this.columnAttributes = this.calculateCTreeAttributes(-1);
        this.rowsFilteredData.forEach(function (rowData) {
            var _a;
            var columnLeaves = (_a = []).concat.apply(_a, _this.columns.map(function (c) { return c.getLeaves(); }));
            var newRow = [];
            columnLeaves.forEach(function (cl) {
                var filteredRows = cl.applyFiltersToData(rowData);
                var reducedRow = _this.reduceFunction(filteredRows);
                newRow.push(cl.getValue(reducedRow));
            });
            if (!_this.fData) {
                _this.fData = [];
            }
            _this.fData.push(newRow);
        });
        this.fData$.next(this.fData);
    };
    /**
     * aggiorna i dati filtrando la tabella fornita precedentemente
     * o, se forniti, applica il filtro sui dati forniti come parametro
     * e li utilizza per le prossime chiamate
     *
     * @memberof TreeTable
     */
    TreeTable.prototype.updateData = function (data, forceFiltersUpdate) {
        var _this = this;
        if (forceFiltersUpdate === void 0) { forceFiltersUpdate = false; }
        var _a, _b, _c;
        if (data !== undefined) {
            this.data = this.dataTransformer(data); // se data è fornito come parametro allora imposta quello per la tabella corrente
        }
        if (data !== undefined || forceFiltersUpdate || this.filterGeneratorsChanged) {
            this.columns = this.columnsFilterGenerator.getFilters(this.data);
            this._columnsLeaves = (_a = []).concat.apply(_a, this.columns.map(function (c) { return c.getLeaves(); }));
            this.rows = this.rowsFilterGenerator.getFilters(this.data);
            this._rowsLeaves = (_b = []).concat.apply(_b, this.rows.map(function (c) { return c.getLeaves(); }));
            this.filterGeneratorsChanged = false;
        }
        if (this.data.length === 0) {
            this.fData = [];
            this.columns = [];
            this.rows = [];
            this._rowsLeaves = [];
            this._columnsLeaves = [];
            this.fData$.next(this.fData);
            return;
        }
        this.rowAttributes = this.calculateRTreeAttributes(-1);
        this.columnAttributes = this.calculateCTreeAttributes(-1);
        var rowLeaves = (_c = []).concat.apply(_c, this.rows.map(function (c) { return c.getLeaves(); }));
        this._rowsLeaves = rowLeaves;
        var step1 = rowLeaves.map(function (rl) {
            return { rowData: rl.getParsedData(_this.data), isTotalRow: rl.isTotalRow };
        });
        this.fData = [];
        this.rowsFilteredData = step1;
        step1.forEach(function (rowData) {
            var _a;
            var columnLeaves = (_a = []).concat.apply(_a, _this.columns.map(function (c) { return c.getLeaves(); }));
            var newRow = [];
            columnLeaves.forEach(function (cl) {
                var filteredRows = cl.applyFiltersToData(rowData);
                var reducedRow = _this.reduceFunction(filteredRows);
                newRow.push(cl.getValue(reducedRow));
            });
            if (!_this.fData) {
                _this.fData = [];
            }
            _this.fData.push(newRow);
        });
        this.fData$.next(this.fData);
    };
    TreeTable.prototype.calculateRTreeAttributes = function (currentLevel) {
        var subLeaves = 0;
        var maxLevel = currentLevel;
        if (this.rows.length === 0) {
            subLeaves = 1;
        }
        else {
            this.rows.forEach(function (sr) {
                var result = sr.calculateAttributes(currentLevel + 1);
                maxLevel = maxLevel < result.maxSubLevel ? result.maxSubLevel : maxLevel;
                subLeaves += result.subLeaves;
            });
            this.rows.forEach(function (sr) {
                sr.propagateTopDownAttributes(sr.subLeaves || 0, maxLevel);
            });
        }
        return { subLeaves: subLeaves, maxSubLevel: maxLevel };
    };
    TreeTable.prototype.calculateCTreeAttributes = function (currentLevel) {
        var subLeaves = 0;
        var maxLevel = currentLevel;
        if (this.columns.length === 0) {
            subLeaves = 1;
        }
        else {
            this.columns.forEach(function (sc) {
                var result = sc.calculateAttributes(currentLevel + 1);
                maxLevel = maxLevel < result.maxSubLevel ? result.maxSubLevel : maxLevel;
                subLeaves += result.subLeaves;
            });
            this.columns.forEach(function (sc) {
                sc.propagateTopDownAttributes(sc.subLeaves || 0, maxLevel);
            });
        }
        return { subLeaves: subLeaves, maxSubLevel: maxLevel };
    };
    return TreeTable;
}());
export { TreeTable };
//# sourceMappingURL=TreeTable.js.map