/**
 * Definizione di una colonna
 *
 * @export
 * @class TreeTableCFilter
 * @template T
 */
var TreeTableCFilter = /** @class */ (function () {
    function TreeTableCFilter(options, data) {
        this.options = options;
        this.expanding = false;
        this.currentSubColumns = this.options.subColumnsGenerator
            ? this.options.subColumnsGenerator.getFilters(data, this)
            : [];
        this.currentCollapsedSubColumns = this.options.collapsedSubColumnsGenerator
            ? this.options.collapsedSubColumnsGenerator.getFilters(data, this)
            : [];
        this.childrenHidden = this.options.hideChildren;
    }
    Object.defineProperty(TreeTableCFilter.prototype, "subColumns", {
        get: function () {
            return this.currentSubColumns.filter(function (f) { return f.options.forceShow; }).concat((!this.hideChildren ? this.currentSubColumns.filter(function (f) { return !f.options.forceShow; }) : []));
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(TreeTableCFilter.prototype, "text", {
        get: function () {
            return this.options.text;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(TreeTableCFilter.prototype, "parent", {
        get: function () {
            return this.options.parent;
        },
        set: function (parent) {
            this.options.parent = parent;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(TreeTableCFilter.prototype, "hideChildren", {
        get: function () {
            return this.childrenHidden;
        },
        set: function (newValue) {
            this.childrenHidden = newValue;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(TreeTableCFilter.prototype, "isLeaf", {
        get: function () {
            return this.subColumns && this.subColumns.length === 0;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(TreeTableCFilter.prototype, "hasSubColumns", {
        get: function () {
            return this.currentSubColumns.length > 0;
        },
        enumerable: true,
        configurable: true
    });
    TreeTableCFilter.prototype.getLeaves = function () {
        var activeFilters = this.subColumns;
        var leaves = [];
        if (activeFilters.length === 0) {
            leaves.push(this);
        }
        else {
            activeFilters.forEach(function (af) {
                af.getLeaves().forEach(function (l) {
                    leaves.push(l);
                });
            });
        }
        return leaves;
    };
    TreeTableCFilter.prototype.getValue = function (row) {
        var baseValue = this.options.reduceFunction(row);
        if (this.options.translatorFunction) {
            baseValue = this.options.translatorFunction(baseValue);
        }
        return baseValue;
    };
    TreeTableCFilter.prototype.clone = function (data, parent) {
        return new TreeTableCFilter({
            text: this.options.text,
            subColumnsGenerator: this.options.subColumnsGenerator
                ? this.options.subColumnsGenerator.clone(data, parent)
                : undefined,
            collapsedSubColumnsGenerator: this.options.collapsedSubColumnsGenerator
                ? this.options.collapsedSubColumnsGenerator.clone(data, parent)
                : undefined,
            hideChildren: this.options.hideChildren,
            forceShow: this.options.forceShow,
            filterFunction: this.options.filterFunction,
            reduceFunction: this.options.reduceFunction,
            translatorFunction: this.options.translatorFunction,
            parent: parent,
        }, data);
    };
    TreeTableCFilter.prototype.applyFiltersToData = function (data) {
        var filters = [this];
        var currentNode = this;
        while (currentNode.parent !== undefined) {
            currentNode = currentNode.parent;
            filters = [currentNode].concat(filters);
        }
        var fData = data.rowData;
        // tslint:disable-next-line:prefer-for-of
        for (var i = 0; i < filters.length; i++) {
            if (filters[i].options.filterFunction !== undefined) {
                fData = fData.filter(filters[i].options.filterFunction);
            }
        }
        return { rowData: fData, isTotalRow: data.isTotalRow };
    };
    TreeTableCFilter.prototype.applyTopFilters = function (row) {
        var filters = [this];
        var currentNode = this;
        while (currentNode.parent !== undefined) {
            currentNode = currentNode.parent;
            filters = [currentNode].concat(filters);
        }
        var validRow = true;
        // tslint:disable-next-line:prefer-for-of
        for (var i = 0; i < filters.length; i++) {
            if (filters[i].options.filterFunction !== undefined) {
                validRow = filters[i].options.filterFunction(row);
            }
        }
        return validRow ? row : undefined;
    };
    TreeTableCFilter.prototype.calculateAttributes = function (currentLevel) {
        this.level = currentLevel;
        var activeFilters = this.subColumns;
        var subLeaves = 0;
        var maxSubLevel = currentLevel;
        if (this.subColumns.length === 0 || activeFilters.length === 0) {
            this.subLeaves = 0;
            subLeaves = 1;
        }
        else {
            this.subColumns.forEach(function (sc) {
                var result = sc.calculateAttributes(currentLevel + 1);
                maxSubLevel = maxSubLevel < result.maxSubLevel ? result.maxSubLevel : maxSubLevel;
                subLeaves += result.subLeaves;
            });
            this.subLeaves = subLeaves;
        }
        this.maxSubLevel = maxSubLevel;
        return { subLeaves: subLeaves, maxSubLevel: maxSubLevel };
    };
    TreeTableCFilter.prototype.propagateTopDownAttributes = function (branchSubLeaves, maxLevel) {
        var activeFilters = this.subColumns;
        this.maxLevel = maxLevel;
        this.branchSubLeaves = branchSubLeaves;
        // tslint:disable-next-line:no-empty
        if (activeFilters.length === 0) {
        }
        else {
            activeFilters.forEach(function (sc) {
                sc.propagateTopDownAttributes(branchSubLeaves, maxLevel);
            });
        }
    };
    return TreeTableCFilter;
}());
export { TreeTableCFilter };
//# sourceMappingURL=TreeTableCFilter.js.map