var ConstantTreeTableRFilterGenerator = /** @class */ (function () {
    function ConstantTreeTableRFilterGenerator(filters) {
        this.filters = filters;
        this.isDynamic = false;
    }
    ConstantTreeTableRFilterGenerator.prototype.getFilters = function (data) {
        return [this.filters];
    };
    return ConstantTreeTableRFilterGenerator;
}());
export { ConstantTreeTableRFilterGenerator };
//# sourceMappingURL=TreeTableRFilterGenerator.js.map