var NUMDECIMALS = 2;
export var ReduceOperationsEnum;
(function (ReduceOperationsEnum) {
    ReduceOperationsEnum[ReduceOperationsEnum["NONE"] = -1] = "NONE";
    ReduceOperationsEnum[ReduceOperationsEnum["CUSTOM"] = 0] = "CUSTOM";
    ReduceOperationsEnum[ReduceOperationsEnum["SUM"] = 1] = "SUM";
    ReduceOperationsEnum[ReduceOperationsEnum["AVG"] = 2] = "AVG";
    ReduceOperationsEnum[ReduceOperationsEnum["FIRST"] = 3] = "FIRST";
    ReduceOperationsEnum[ReduceOperationsEnum["LAST"] = 4] = "LAST";
    ReduceOperationsEnum[ReduceOperationsEnum["DISTINCT"] = 5] = "DISTINCT";
})(ReduceOperationsEnum || (ReduceOperationsEnum = {}));
export var ColumnDataType;
(function (ColumnDataType) {
    ColumnDataType[ColumnDataType["INTEGER"] = 0] = "INTEGER";
    ColumnDataType[ColumnDataType["FLOAT"] = 1] = "FLOAT";
    ColumnDataType[ColumnDataType["STRING"] = 2] = "STRING";
})(ColumnDataType || (ColumnDataType = {}));
export function reduceFunctionFromColumnsDefinitions(columns) {
    return function (data) {
        var returnRow = {};
        columns.forEach(function (col) {
            // TODO retrieve operations
            if (col.operation > 0) {
                returnRow[col.columnName] = getBaseReduceFunctionFromOperation(col.columnName, col.operation, col.options)(data);
            }
            else if (col.operation === 0) {
                if (col.customOperation) {
                    returnRow[col.columnName] = col.customOperation(data); // TODO error checking
                }
            }
        });
        return returnRow;
    };
}
function getBaseReduceFunctionFromOperation(columnName, operation, options) {
    switch (operation // TODO operations
    ) {
        case ReduceOperationsEnum.SUM:
            return sumGenerator(columnName, options);
        case ReduceOperationsEnum.AVG:
            return avgGenerator(columnName, options);
        case ReduceOperationsEnum.DISTINCT:
            return distinctGenerator(columnName, options);
        default:
            return function () { return 'reducer not implemented'; };
    }
}
function distinctGenerator(columnName, options) {
    return function (data) {
        var currentKeys = new Map();
        if (data.rowData.length === 0) {
            return 0;
        }
        data.rowData.forEach(function (d) {
            var path = (options.uniqueColumnsNames || [])
                // tslint:disable-next-line:no-unsafe-any
                .map(function (cn) { return d[cn]; })
                .join('.');
            if (!currentKeys.has(path)) {
                currentKeys.set(path, 1);
            }
            else {
                currentKeys.set(path, (currentKeys.get(path) || 1) + 1);
            }
        });
        return options.hideNull && currentKeys.size === 0 ? '' : currentKeys.size;
    };
}
function sumGenerator(columnName, options) {
    return function (data) {
        var sValue = 0;
        if (data.rowData.length === 0) {
            return '';
        }
        data.rowData.forEach(function (d) {
            if (options.datatype === ColumnDataType.FLOAT || options.datatype === ColumnDataType.INTEGER) {
                // tslint:disable-next-line:no-unsafe-any
                sValue += d[options.referenceColumnName || columnName];
            }
            else {
                throw new Error('Non è possibile effettuare una somma su dati non FLOAT o INTEGER');
            }
        });
        return options.round ? sValue.toFixed(options.decimals !== undefined ? options.decimals : NUMDECIMALS) : sValue;
    };
}
function avgGenerator(columnName, options) {
    return function (data) {
        if (data.rowData.length === 0) {
            return '';
        }
        var sValue = 0;
        data.rowData.forEach(function (d) {
            if (options.datatype === ColumnDataType.FLOAT || options.datatype === ColumnDataType.INTEGER) {
                // tslint:disable-next-line:no-unsafe-any
                sValue += d[options.referenceColumnName || columnName];
            }
            else {
                throw new Error('Non è possibile effettuare una media su dati non FLOAT o INTEGER');
            }
        });
        return options.round
            ? (sValue / data.rowData.length).toFixed(options.decimals !== undefined ? options.decimals : NUMDECIMALS)
            : sValue / data.rowData.length;
    };
}
//# sourceMappingURL=Reducers.js.map