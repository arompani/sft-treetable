import { stringSortAscending, stringSortDescending } from '../Sorting';
import { TreeTableRFilter } from '../TreeTableRFilter';
var DistinctTreeTableRFilterGenerator = /** @class */ (function () {
    function DistinctTreeTableRFilterGenerator(options) {
        this.options = options;
        this.isDynamic = false;
    }
    DistinctTreeTableRFilterGenerator.prototype.getFilters = function (data, parent) {
        var _this = this;
        var names = [];
        data.forEach(function (d) {
            if (names.find(function (n) { return n === d[_this.options.columnName]; }) === undefined) {
                names.push(d[_this.options.columnName]);
            }
        });
        if (this.options.sort) {
            if (this.options.sortFunction) {
                names.sort(this.options.sortFunction);
            }
            else {
                names.sort(this.options.sortDescending ? stringSortDescending : stringSortAscending);
            }
        }
        var ret = names.map(function (n) {
            return new TreeTableRFilter({
                subGenerator: _this.options.subGenerator,
                text: _this.options.translatorFunction ? _this.options.translatorFunction(n) : n,
                hideChildren: _this.options.hideChildren,
                forceShow: _this.options.forceShow,
                filterFunction: function (row) { return row[_this.options.columnName] === n; },
                parent: parent,
            }, data.filter(function (row) { return row[_this.options.columnName] === n; }));
        });
        if (!this.options.hideTotals) {
            ret.push(new TreeTableRFilter({
                subGenerator: undefined,
                text: "Totale " + (parent ? " " + parent.text : ''),
                hideChildren: this.options.hideChildren,
                forceShow: this.options.forceShow,
                filterFunction: parent ? parent.filterFunction : function (row) { return true; },
                parent: parent,
                isTotalRow: true,
            }, data.filter(parent ? parent.filterFunction : function (row) { return true; })));
        }
        return ret;
    };
    return DistinctTreeTableRFilterGenerator;
}());
export { DistinctTreeTableRFilterGenerator };
//# sourceMappingURL=DistinctTreeTableRFilterGenerator.js.map