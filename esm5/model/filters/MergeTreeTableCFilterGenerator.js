var MergeTreeTableCFilterGenerator = /** @class */ (function () {
    function MergeTreeTableCFilterGenerator(filters) {
        this.filters = filters;
        this.isDynamic = false;
    }
    MergeTreeTableCFilterGenerator.prototype.getFilters = function (data, parent) {
        var newFilters = [].concat.apply([], this.filters.map(function (f) { return f.getFilters(data, parent); }));
        newFilters.forEach(function (f) {
            f.parent = parent;
        }); // update parents on filters
        return newFilters;
    };
    MergeTreeTableCFilterGenerator.prototype.clone = function (data, parent) {
        return new MergeTreeTableCFilterGenerator([].concat
            .apply([], this.filters)
            .map(function (f) { return f.clone(data, parent); }));
    };
    return MergeTreeTableCFilterGenerator;
}());
export { MergeTreeTableCFilterGenerator };
//# sourceMappingURL=MergeTreeTableCFilterGenerator.js.map