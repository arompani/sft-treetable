import { stringSortAscending, stringSortDescending } from '../Sorting';
import { TreeTableCFilter } from '../TreeTableCFilter';
var DistinctTreeTableCFilterGenerator = /** @class */ (function () {
    function DistinctTreeTableCFilterGenerator(options) {
        this.options = options;
        this.isDynamic = false;
    }
    DistinctTreeTableCFilterGenerator.prototype.getFilters = function (data, parent) {
        var _this = this;
        var names = [];
        data.forEach(function (d) {
            if (names.find(function (n) { return n === d[_this.options.columnName]; }) === undefined) {
                names.push(d[_this.options.columnName]);
            }
        });
        if (this.options.sort) {
            if (this.options.sortFunction) {
                names.sort(this.options.sortFunction);
            }
            else {
                names.sort(this.options.sortDescending ? stringSortDescending : stringSortAscending);
            }
        }
        return names.map(function (n) {
            return new TreeTableCFilter({
                text: _this.options.translatorFunction ? _this.options.translatorFunction(n) : n,
                subColumnsGenerator: _this.options.subColumnsGenerator,
                collapsedSubColumnsGenerator: _this.options.collapsedSubColumnsGenerator,
                hideChildren: _this.options.hideChildren,
                forceShow: _this.options.forceShow,
                reduceFunction: _this.options.reduceFunction,
                filterFunction: function (row) { return row[_this.options.columnName] === n; },
                parent: parent,
            }, data.filter(function (column) { return column[_this.options.columnName] === n; }));
        });
    };
    DistinctTreeTableCFilterGenerator.prototype.clone = function (data, parent) {
        return new DistinctTreeTableCFilterGenerator({
            columnName: this.options.columnName,
            subColumnsGenerator: this.options.subColumnsGenerator
                ? this.options.subColumnsGenerator.clone(data, parent)
                : undefined,
            collapsedSubColumnsGenerator: this.options.collapsedSubColumnsGenerator
                ? this.options.collapsedSubColumnsGenerator.clone(data, parent)
                : undefined,
            hideChildren: this.options.hideChildren,
            forceShow: this.options.forceShow,
            translatorFunction: this.options.translatorFunction,
            reduceFunction: this.options.reduceFunction,
        });
    };
    return DistinctTreeTableCFilterGenerator;
}());
export { DistinctTreeTableCFilterGenerator };
//# sourceMappingURL=DistinctTreeTableCFilterGenerator.js.map