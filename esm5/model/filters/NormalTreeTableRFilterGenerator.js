import { TreeTableRFilter } from '../TreeTableRFilter';
var NormalTreeTableRFilterGenerator = /** @class */ (function () {
    function NormalTreeTableRFilterGenerator(options) {
        this.options = options;
        this.isDynamic = false;
    }
    NormalTreeTableRFilterGenerator.prototype.getFilters = function (data, parent) {
        var _this = this;
        var ret = data.map(function (d) {
            return new TreeTableRFilter({
                subGenerator: _this.options.subGenerator,
                text: d[_this.options.textBinding],
                hideChildren: true,
                forceShow: false,
                filterFunction: function (row) { return row === d; },
                parent: parent,
            }, data);
        });
        return ret;
    };
    return NormalTreeTableRFilterGenerator;
}());
export { NormalTreeTableRFilterGenerator };
//# sourceMappingURL=NormalTreeTableRFilterGenerator.js.map