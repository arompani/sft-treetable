import { Subject } from 'rxjs';
var SftTreeTableComponent = /** @class */ (function () {
    function SftTreeTableComponent(treeTable) {
        this.rightMatrix = new Subject();
        this.leftMatrix = new Subject();
        this.totalsColumn = [];
        this.treeTable = treeTable;
    }
    SftTreeTableComponent.prototype.ngAfterViewInit = function () {
        var _this = this;
        var onNewValue = function (value) {
            setTimeout(function () {
                if (_this.treeTable.rowAttributes !== undefined && _this.treeTable.columnAttributes !== undefined) {
                    _this.totalsColumn = _this.generateTotalsRowFlags(_this.treeTable.rowsLeaves);
                    _this.leftMatrix.next(_this.generateRowHeadersMatrix(_this.treeTable.rowsTree, _this.treeTable.rowsLeaves, _this.treeTable.rowAttributes.maxSubLevel, _this.treeTable.columnAttributes.maxSubLevel));
                    _this.rightMatrix.next(_this.generateHeaderAndDataMatrix(_this.treeTable.columnsTree, _this.treeTable.columnsLeaves, value, _this.totalsColumn));
                }
            }, 0);
        };
        this.treeTable.filteredData$.subscribe(onNewValue.bind(this));
    };
    SftTreeTableComponent.prototype.columnClick = function (element) {
        if (element.columnFilter && !element.placeholder) {
            if (element.columnFilter.hasSubColumns) {
                element.expanding = true;
                element.columnFilter.hideChildren = !element.columnFilter.hideChildren;
                this.treeTable.updateDataAfterColumnsChange();
            }
        }
    };
    SftTreeTableComponent.prototype.rowClick = function (element) {
        if (element.rowFilter) {
            if (element.rowFilter.hasSubRows) {
                element.rowFilter.hideChildren = !element.rowFilter.hideChildren;
                this.treeTable.updateData();
            }
        }
    };
    SftTreeTableComponent.prototype.toggleChildren = function (columnNode) {
        columnNode.hideChildren = !columnNode.hideChildren;
        this.treeTable.updateData();
    };
    SftTreeTableComponent.prototype.toggleRChildren = function (rowNode) {
        rowNode.hideChildren = !rowNode.hideChildren;
        this.treeTable.updateData();
    };
    /**
     * fornisce una colonna in ogni cella indica se la riga corrispondente è una riga di totali o no
     *
     * @param {TreeTableRFilter<T>[]} rowsLeaves
     * @returns {boolean[]}
     * @memberof SftTreeTableComponent
     */
    SftTreeTableComponent.prototype.generateTotalsRowFlags = function (rowsLeaves) {
        return rowsLeaves.map(function (rl) { return (rl.isTotalRow ? true : false); });
    };
    /**
     * Genera la matrice di colonne della zona a sinistra della tabella, ovvero quella contenente le definizioni delle
     * intestazioni delle righe
     *
     * @param {TreeTableRFilter<T>[]} rowsTree
     * @param {TreeTableRFilter<T>[]} rowsLeaves
     * @memberof SftTreeTableComponent
     */
    SftTreeTableComponent.prototype.generateRowHeadersMatrix = function (rowsTree, rowsLeaves, maxLevel, columnsMaxLevel) {
        var _this = this;
        // il numero di colonne è pari al massimo livello trovato per i filtri
        var matrix = [];
        for (var i = 0; i < maxLevel + 1; i++) {
            matrix.push([]);
        }
        for (var i = 0; i < maxLevel + 1; i++) {
            for (var g = 0; g < columnsMaxLevel + 1; g++) {
                matrix[i].push({
                    width: 1,
                    height: 1,
                    placeholder: true,
                    text: '',
                    topleft: true,
                });
            }
        }
        // è possibile che capiti una foglia non al massimo livello, questo significa che
        // devono essere creati dei placeholder sia per larghezze >1 che per altezze >1
        // imposto la matrice delle intestazioni di riga tramite un depth first sull'albero
        //  delle intestazioni
        rowsTree.forEach(function (filter) {
            _this.insertRowBranch(filter, matrix);
        });
        return matrix;
    };
    /**
     * Aggiunge il branch del filtro di riga all'interno della matrice delle celle
     *
     * @param {TreeTableRFilter<T>} rowFilter
     * @param {ICell[][]} [matrix]
     * @memberof SftTreeTableComponent
     */
    SftTreeTableComponent.prototype.insertRowBranch = function (filter, matrix) {
        var _this = this;
        var height = filter.isLeaf ? 1 : filter.subLeaves || 0;
        var width = filter.isLeaf ? (filter.maxLevel || 0) - (filter.level || 1) + 1 : 1;
        for (var g = 0; g < width; g++) {
            for (var i = 0; i < height; i++) {
                matrix[(filter.level || 0) + g].push({
                    height: height,
                    width: width,
                    placeholder: i !== 0 || g !== 0,
                    text: filter.text,
                    spacer: g === 0 && i !== 0,
                    rowFilter: filter,
                    isTotal: filter.isTotalRow,
                });
            }
        }
        filter.subRows.forEach(function (subFilter) {
            _this.insertRowBranch(subFilter, matrix);
        });
    };
    /**
     * Genera la matrice della zona destra della tabella, contenente la intestazione e le righe di dati
     * (attenzione che quella che fornisce è una matrice di colonne, non di righe, questo perchè la UI si basa
     * sulla visualizzazione di colonne)
     *
     * @param {TreeTableCFilter<T>[]} columnsTree
     * @param {TreeTableCFilter<T>[]} columnsLeaves
     * @param {((string | number)[][])} data
     * @memberof SftTreeTableComponent
     */
    SftTreeTableComponent.prototype.generateHeaderAndDataMatrix = function (columnsTree, columnsLeaves, data, totalsColumn) {
        var headerMatrix = this.generateHeaderMatrix(columnsTree, columnsLeaves);
        var _loop_1 = function (i) {
            // recupero la colonna dai dati filtrati
            var dataColumn = data.map(function (r) { return r[i]; }); // TODO migliorare mettendo tutto in una sola chiamata
            // per ogni dato della colonna lo aggiungo alla colonna sotto gli elementi della intestazione
            dataColumn.forEach(function (dc, index) {
                headerMatrix[i].push({
                    height: 1,
                    width: 1,
                    placeholder: false,
                    text: dc,
                    isTotal: totalsColumn[index],
                });
            });
        };
        for (var i = 0; i < columnsLeaves.length; i++) {
            _loop_1(i);
        }
        return headerMatrix;
    };
    /**
     * Genera una matrice rettangolare di oggetti che rispecchia la struttura visiva della intestazione della tabella
     * (non contiene i dati)
     *
     * @param {TreeTableCFilter<T>[]} columnsTree
     * @param {TreeTableCFilter<T>[]} columnsLeaves
     * @memberof SftTreeTableComponent
     */
    SftTreeTableComponent.prototype.generateHeaderMatrix = function (columnsTree, columnsLeaves) {
        var levels = [];
        var matrix;
        this.generateColumnsLevelsMatrix(columnsTree, levels);
        // con la matrice contenente ogni foglia per livello occorre creare la matrice finale della intestazione.
        // la matrice dei livelli va convertita in una visione a colonne (ora è a righe).
        // per ogni livello bisogna considerare, per la dimensione orizzontale il numero di foglie che il filtro
        // ha al termine del suo ramo, mentre verticalmente l'altezza è decisa, se il filtro è una foglia,
        // dal suo valore di livello rispetto al livello massimo presente nell'albero, altrimenti ha altezza unitaria
        // inizializzazione della matrice con il numero di colonne necessarie
        matrix = columnsLeaves.map(function (g) { return []; });
        var lockedColumns = [];
        // aggiunta celle della matrice
        levels.forEach(function (levelObjects) {
            // per ogni livello inserisco gli elementi della riga
            var currentColumn = 0;
            var _loop_2 = function (i) {
                if (lockedColumns.find(function (val) { return val === i; }) === undefined) {
                    currentColumn = i;
                    return "break";
                }
            };
            // trovo la prima colonna non bloccata
            for (var i = 0; i < matrix.length; i++) {
                var state_1 = _loop_2(i);
                if (state_1 === "break")
                    break;
            }
            levelObjects.forEach(function (filter) {
                // ogni elemento controllo se è una foglia, e, se necessario ne calcolo il numero di
                // colonne che occuperà
                var height = filter.isLeaf ? (filter.maxLevel || 0) - (filter.level || 0) + 1 : 1;
                if (height > 1) {
                    // se l'altezza è maggiore di 1 allora blocco la colonna da qualsiasi inserimento
                    lockedColumns.push(currentColumn);
                }
                // inserisco la cella corrispondente
                // la larghezza è data dal numero di foglie presenti nel sottoramo del filtro stesso
                matrix[currentColumn].push({
                    placeholder: false,
                    height: height,
                    width: filter.subLeaves || 0,
                    text: filter.text,
                    columnFilter: filter,
                });
                // per semplificare il posizionamento e mantenere la struttura a celle è necessario inserire
                // dei placeholder se la altezza della colonna è maggiore di 1 (questi placeholdere non verranno
                // poi visualizzati nella tabella finale)
                for (var i = 1; i < height; i++) {
                    matrix[currentColumn].push({
                        placeholder: true,
                        height: 1,
                        width: 1,
                        text: filter.text,
                        spacer: true,
                        columnFilter: filter,
                    });
                }
                var _loop_3 = function (i) {
                    if (lockedColumns.find(function (val) { return val === i; }) === undefined) {
                        currentColumn = i;
                        return "break";
                    }
                };
                // ho inserito il testo nella cella corretta che però deve aggiungere se stesso anche nelle colonne
                //  successive se non è una foglia per mantenere la struttura visiva ad albero
                // trovo la prima colonna non bloccata
                for (var i = currentColumn + 1; i < matrix.length; i++) {
                    var state_2 = _loop_3(i);
                    if (state_2 === "break")
                        break;
                }
                for (var i = 1; i < (filter.subLeaves || 0); i++) {
                    // procede ad aggiungere celle alle colonne successive fino a che non ne ha aggiunti un numero
                    // pari alla quantità di foglie che il suo sottoramo contiene
                    matrix[currentColumn].push({
                        placeholder: true,
                        height: height,
                        width: 1,
                        text: filter.text,
                        columnFilter: filter,
                    });
                    currentColumn++;
                }
            });
        });
        return matrix;
    };
    /**
     * Genera un array bidimensionale in cui la prima coordinata è il livello nell'albero
     *
     * @param {TreeTableCFilter<T>[]} columnsTree
     * @param {TreeTableCFilter<T>[][]} currentMatrix
     * @memberof SftTreeTableComponent
     */
    SftTreeTableComponent.prototype.generateColumnsLevelsMatrix = function (columnsTree, currentMatrix) {
        var _this = this;
        columnsTree.forEach(function (filter) {
            if ((filter.level || 0) >= currentMatrix.length) {
                currentMatrix.push([filter]);
            }
            else {
                currentMatrix[filter.level || 0].push(filter);
            }
            if (filter.subColumns.length > 0) {
                _this.generateColumnsLevelsMatrix(filter.subColumns, currentMatrix);
            }
        });
    };
    return SftTreeTableComponent;
}());
export { SftTreeTableComponent };
//# sourceMappingURL=sft-tree-table.js.map