import { IDataEntry } from './IDataEntry';
import { TreeTableCFilter } from './TreeTableCFilter';
export interface ITreeTableCFilterGenerator<T extends IDataEntry> {
    isDynamic: boolean;
    getFilters(data: T[], parent?: TreeTableCFilter<T>): Array<TreeTableCFilter<T>>;
    clone(data: T[], parent: TreeTableCFilter<T>): ITreeTableCFilterGenerator<T>;
}
//# sourceMappingURL=ITreeTableCFilterGenerator.d.ts.map