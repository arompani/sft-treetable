import { IDataEntry } from './IDataEntry';
import { ITreeTableRFilterGenerator } from './TreeTableRFilterGenerator';
/**
 * Definizione di una riga
 *
 * @export
 * @class TreeTableRFilter
 * @template T
 */
export declare class TreeTableRFilter<T extends IDataEntry> {
    private options;
    /**
     * livello del nodo corrente nell'albero (parte da zero)
     *
     *
     * @memberof TreeTableCFilter
     */
    level?: number;
    /**
     * numero di foglie presenti sotto questo nodo
     *
     *
     * @memberof TreeTableCFilter
     */
    subLeaves?: number;
    /**
     * Massimo livello dei nodi figli
     *
     *
     * @memberof TreeTableCFilter
     */
    maxSubLevel?: number;
    /**
     * Massimo livello incontrato in tutto l'albero
     *
     *
     * @memberof TreeTableCFilter
     */
    maxLevel?: number;
    /**
     * numero di foglie visibili del branch a cui questo nodo appartiene
     * (ogni nodo di primo livello dell'albero forma un branch), non è il numero di
     * foglie sotto questo albero ma all'intero branch
     *
     *
     * @memberof TreeTableCFilter
     */
    branchSubLeaves?: number;
    private childrenHidden;
    private currentSubRows;
    constructor(options: {
        subGenerator?: ITreeTableRFilterGenerator<T>;
        text: string;
        hideChildren: boolean;
        forceShow: boolean;
        filterFunction: ((row: T) => boolean);
        parent?: TreeTableRFilter<T>;
        isTotalRow?: boolean;
    }, data: T[]);
    readonly text: string | number;
    readonly subRows: Array<TreeTableRFilter<T>>;
    parent: TreeTableRFilter<T> | undefined;
    hideChildren: boolean;
    /**
     * true se il filtro ha sottocolonne (senza contare quelle dovute al collapsed generator)
     *
     * @readonly
     * @type {boolean}
     * @memberof TreeTableRFilter
     */
    readonly hasSubRows: boolean;
    readonly isLeaf: boolean;
    readonly isTotalRow: boolean | undefined;
    readonly filterFunction: ((row: T) => boolean);
    getLeaves(): Array<TreeTableRFilter<T>>;
    getParsedData(data: T[]): T[];
    applyTopFilters(data: T[]): T[];
    calculateAttributes(currentLevel: number): {
        subLeaves: number;
        maxSubLevel: number;
    };
    propagateTopDownAttributes(branchSubLeaves: number, maxLevel: number): void;
}
//# sourceMappingURL=TreeTableRFilter.d.ts.map