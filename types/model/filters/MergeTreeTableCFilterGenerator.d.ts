import { IDataEntry } from '../IDataEntry';
import { ITreeTableCFilterGenerator } from '../ITreeTableCFilterGenerator';
import { TreeTableCFilter } from '../TreeTableCFilter';
export declare class MergeTreeTableCFilterGenerator<T extends IDataEntry> implements ITreeTableCFilterGenerator<T> {
    private filters;
    isDynamic: boolean;
    constructor(filters: Array<ITreeTableCFilterGenerator<T>>);
    getFilters(data: T[], parent: TreeTableCFilter<T>): Array<TreeTableCFilter<T>>;
    clone(data: T[], parent: TreeTableCFilter<T>): ITreeTableCFilterGenerator<T>;
}
//# sourceMappingURL=MergeTreeTableCFilterGenerator.d.ts.map