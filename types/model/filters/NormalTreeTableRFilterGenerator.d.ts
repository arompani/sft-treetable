import { IDataEntry } from '../IDataEntry';
import { TreeTableRFilter } from '../TreeTableRFilter';
import { ITreeTableRFilterGenerator } from '../TreeTableRFilterGenerator';
export declare class NormalTreeTableRFilterGenerator<T extends IDataEntry> implements ITreeTableRFilterGenerator<T> {
    private options;
    isDynamic: boolean;
    constructor(options: {
        textBinding: string;
        subGenerator: ITreeTableRFilterGenerator<T>;
        hideChildren: boolean;
        forceShow: boolean;
        reduceFunction: ((array: T[]) => T);
    });
    getFilters(data: T[], parent: TreeTableRFilter<T>): Array<TreeTableRFilter<T>>;
}
//# sourceMappingURL=NormalTreeTableRFilterGenerator.d.ts.map