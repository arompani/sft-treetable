import { IDataEntry } from '../IDataEntry';
import { ITreeTableCFilterGenerator } from '../ITreeTableCFilterGenerator';
import { TreeTableCFilter } from '../TreeTableCFilter';
export declare class DistinctTreeTableCFilterGenerator<T extends IDataEntry> implements ITreeTableCFilterGenerator<T> {
    private options;
    isDynamic: boolean;
    constructor(options: {
        columnName: string;
        subColumnsGenerator?: ITreeTableCFilterGenerator<T>;
        collapsedSubColumnsGenerator?: ITreeTableCFilterGenerator<T>;
        hideChildren: boolean;
        forceShow: boolean;
        sort?: boolean;
        sortDescending?: boolean;
        sortFunction?: ((a: string, b: string) => number);
        translatorFunction?: ((value: string) => string);
        reduceFunction: ((arg: T) => string | number);
    });
    getFilters(data: T[], parent: TreeTableCFilter<T>): Array<TreeTableCFilter<T>>;
    clone(data: T[], parent: TreeTableCFilter<T>): ITreeTableCFilterGenerator<T>;
}
//# sourceMappingURL=DistinctTreeTableCFilterGenerator.d.ts.map