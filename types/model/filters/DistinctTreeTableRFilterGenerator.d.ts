import { IDataEntry } from '../IDataEntry';
import { TreeTableRFilter } from '../TreeTableRFilter';
import { ITreeTableRFilterGenerator } from '../TreeTableRFilterGenerator';
export declare class DistinctTreeTableRFilterGenerator<T extends IDataEntry> implements ITreeTableRFilterGenerator<T> {
    private options;
    isDynamic: boolean;
    constructor(options: {
        columnName: string;
        subGenerator: ITreeTableRFilterGenerator<T>;
        hideChildren: boolean;
        forceShow: boolean;
        sort?: boolean;
        sortDescending?: boolean;
        sortFunction?: ((a: string, b: string) => number);
        translatorFunction?: ((value: string) => string);
        hideTotals?: boolean;
    });
    getFilters(data: T[], parent: TreeTableRFilter<T>): Array<TreeTableRFilter<T>>;
}
//# sourceMappingURL=DistinctTreeTableRFilterGenerator.d.ts.map