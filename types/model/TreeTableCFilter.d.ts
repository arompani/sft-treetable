import { IDataEntry } from './IDataEntry';
import { ITreeTableCFilterGenerator } from './ITreeTableCFilterGenerator';
/**
 * Definizione di una colonna
 *
 * @export
 * @class TreeTableCFilter
 * @template T
 */
export declare class TreeTableCFilter<T extends IDataEntry> {
    private options;
    /**
     * livello del nodo corrente nell'albero (parte da zero)
     *
     *
     * @memberof TreeTableCFilter
     */
    level?: number;
    /**
     * numero di foglie presenti sotto questo nodo
     *
     *
     * @memberof TreeTableCFilter
     */
    subLeaves?: number;
    /**
     * Massimo livello dei nodi figli
     *
     *
     * @memberof TreeTableCFilter
     */
    maxSubLevel?: number;
    /**
     * Massimo livello incontrato in tutto l'albero
     *
     *
     * @memberof TreeTableCFilter
     */
    maxLevel?: number;
    /**
     * numero di foglie visibili del branch a cui questo nodo appartiene
     * (ogni nodo di primo livello dell'albero forma un branch), non è il numero di
     * foglie sotto questo albero ma all'intero branch
     *
     *
     * @memberof TreeTableCFilter
     */
    branchSubLeaves?: number;
    expanding: boolean;
    private childrenHidden;
    private currentSubColumns;
    private currentCollapsedSubColumns?;
    constructor(options: {
        text?: string;
        subColumnsGenerator?: ITreeTableCFilterGenerator<T>;
        collapsedSubColumnsGenerator?: ITreeTableCFilterGenerator<T>;
        hideChildren: boolean;
        forceShow: boolean;
        filterFunction?: ((row: T) => boolean);
        reduceFunction: ((row: T) => string | number);
        parent?: TreeTableCFilter<T>;
        translatorFunction?: (value: string | number | undefined) => string | number;
    }, data: T[]);
    readonly subColumns: Array<TreeTableCFilter<T>>;
    readonly text: string | undefined;
    parent: TreeTableCFilter<T> | undefined;
    hideChildren: boolean;
    readonly isLeaf: boolean;
    readonly hasSubColumns: boolean;
    getLeaves(): Array<TreeTableCFilter<T>>;
    getValue(row: T): string | number;
    clone(data: T[], parent: TreeTableCFilter<T>): TreeTableCFilter<T>;
    applyFiltersToData(data: {
        rowData: T[];
        isTotalRow?: boolean;
    }): {
        rowData: T[];
        isTotalRow?: boolean;
    };
    applyTopFilters(row: T): T | undefined;
    calculateAttributes(currentLevel: number): {
        subLeaves: number;
        maxSubLevel: number;
    };
    propagateTopDownAttributes(branchSubLeaves: number, maxLevel: number): void;
}
//# sourceMappingURL=TreeTableCFilter.d.ts.map