import { IDataEntry } from './IDataEntry';
export declare enum ReduceOperationsEnum {
    NONE = -1,
    CUSTOM = 0,
    SUM = 1,
    AVG = 2,
    FIRST = 3,
    LAST = 4,
    DISTINCT = 5
}
export declare enum ColumnDataType {
    INTEGER = 0,
    FLOAT = 1,
    STRING = 2
}
export interface IOperationOptions {
    decimals?: number;
    round: boolean;
    datatype: ColumnDataType;
    hideNull?: boolean;
    uniqueColumnsNames?: string[];
    referenceColumnName?: string;
}
export interface IReduceColumnsDefinition<T extends IDataEntry> {
    columnName: string;
    operation: ReduceOperationsEnum | number;
    customOperation?: ((data: {
        rowData: T[];
        isTotalRow?: boolean;
    }) => string);
    options: IOperationOptions;
}
export declare function reduceFunctionFromColumnsDefinitions<T extends IDataEntry>(columns: Array<IReduceColumnsDefinition<T>>): ((data: {
    rowData: T[];
    isTotalRow?: boolean;
}) => T);
//# sourceMappingURL=Reducers.d.ts.map