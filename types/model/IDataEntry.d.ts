/**
 * Definizione di una generica riga
 *
 * @export
 * @interface IDataEntry
 */
export interface IDataEntry {
    [key: string]: any;
}
//# sourceMappingURL=IDataEntry.d.ts.map