import { Observable } from 'rxjs';
import { IDataEntry } from './IDataEntry';
import { ITreeTableCFilterGenerator } from './ITreeTableCFilterGenerator';
import { TreeTableCFilter } from './TreeTableCFilter';
import { TreeTableRFilter } from './TreeTableRFilter';
import { ITreeTableRFilterGenerator } from './TreeTableRFilterGenerator';
/**
 * Gestione delle righe e colonne della tabella e filtraggio
 *
 * @export
 * @class TreeTable
 * @template T
 */
export declare class TreeTable<T extends IDataEntry> {
    private rowsFilterGenerator;
    private columnsFilterGenerator;
    private dataTransformer;
    private reduceFunction;
    rowAttributes: {
        subLeaves: number;
        maxSubLevel: number;
    } | undefined;
    columnAttributes: {
        subLeaves: number;
        maxSubLevel: number;
    } | undefined;
    private columns;
    private _columnsLeaves;
    private rows;
    private _rowsLeaves;
    private data;
    private fData$;
    private fData?;
    private filterGeneratorsChanged;
    private rowsFilteredData;
    constructor(rowsFilterGenerator: ITreeTableRFilterGenerator<T>, columnsFilterGenerator: ITreeTableCFilterGenerator<T>, dataTransformer: ((data: IDataEntry[]) => T[]), reduceFunction: ((data: {
        rowData: T[];
        isTotalRow?: boolean;
    }) => T));
    /**
     * cambia gli attuali generatori di filtri delle righe
     *
     * @memberof TreeTable
     */
    rowFiltersGenerator: ITreeTableRFilterGenerator<T>;
    /**
     * cambia gli attuali generatori di filtri delle colonne
     *
     * @memberof TreeTable
     */
    columnFiltersGenerator: ITreeTableCFilterGenerator<T>;
    /**
     * cambia la funzione di riduzione applicata
     *
     * @memberof TreeTable
     */
    currentReduceFunction: ((data: {
        rowData: T[];
        isTotalRow?: boolean;
    }) => T);
    readonly filteredData$: Observable<Array<Array<string | number>>>;
    readonly filteredData: (string | number)[][] | undefined;
    /**
     * fornisce l'albero completo delle colonne
     *
     * @readonly
     * @type {TreeTableCFilter<T>}
     * @memberof TreeTable
     */
    /**
    * cambia l'albero delle colonne con quello indicato ed avvia la rigenerazione della
    * tabella dei dati filtrati
    *
    * @memberof TreeTable
    */
    columnsTree: Array<TreeTableCFilter<T>>;
    readonly columnsLeaves: Array<TreeTableCFilter<T>>;
    /**
     * fornisce l'albero completo delle righe
     *
     * @readonly
     * @type {TreeTableRFilter<T>}
     * @memberof TreeTable
     */
    rowsTree: Array<TreeTableRFilter<T>>;
    readonly rowsLeaves: Array<TreeTableRFilter<T>>;
    updateDataAfterColumnsChange(): void;
    /**
     * aggiorna i dati filtrando la tabella fornita precedentemente
     * o, se forniti, applica il filtro sui dati forniti come parametro
     * e li utilizza per le prossime chiamate
     *
     * @memberof TreeTable
     */
    updateData(data?: IDataEntry[], forceFiltersUpdate?: boolean): void;
    calculateRTreeAttributes(currentLevel: number): {
        subLeaves: number;
        maxSubLevel: number;
    };
    calculateCTreeAttributes(currentLevel: number): {
        subLeaves: number;
        maxSubLevel: number;
    };
}
//# sourceMappingURL=TreeTable.d.ts.map