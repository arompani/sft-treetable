import { IDataEntry } from './IDataEntry';
import { TreeTableRFilter } from './TreeTableRFilter';
export interface ITreeTableRFilterGenerator<T extends IDataEntry> {
    isDynamic: boolean;
    getFilters(data: T[], parent?: TreeTableRFilter<T>): Array<TreeTableRFilter<T>>;
}
export declare class ConstantTreeTableRFilterGenerator<T extends IDataEntry> implements ITreeTableRFilterGenerator<T> {
    private filters;
    isDynamic: boolean;
    constructor(filters: TreeTableRFilter<T>);
    getFilters(data: T[]): Array<TreeTableRFilter<T>>;
}
//# sourceMappingURL=TreeTableRFilterGenerator.d.ts.map