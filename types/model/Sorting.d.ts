import { IDataEntry } from './IDataEntry';
export declare function generateSortDescending<T extends IDataEntry>(columnName: string): ((a: T, b: T) => number);
export declare function generateSortAscending<T extends IDataEntry>(columnName: string): ((a: T, b: T) => number);
export declare function stringSortAscending<T extends IDataEntry>(a: string, b: string): number;
export declare function stringSortDescending<T extends IDataEntry>(a: string, b: string): number;
//# sourceMappingURL=Sorting.d.ts.map