export interface DataExportColumn {
    header: string;
    format: 'none' | 'currency' | 'date' | 'time' | 'datetime' | 'upper' | 'lower' | 'integer' | 'nullzero' | 'icell';
    formatFunction?: (data: any) => any;
    excelDataFormat?: string;
    headerCssClasses: string;
    dataCssClasses: string;
    hideIfNull?: boolean;
}
export interface DataExportTotalRow {
    rowCssClass: string;
    columns: Array<{
        index: number;
        reduceFunction: (pv: any, cv: any) => any;
        format: 'none' | 'currency' | 'date' | 'time' | 'datetime' | 'upper' | 'lower' | 'integer' | 'nullzero' | 'icell';
    }>;
    textInsertFunctions: Array<(row: any[]) => any[]>;
}
export interface DataExportDocumentElement {
    type: 'title' | 'table';
}
export interface DataExportDataRowOptions {
    isTotalRow?: boolean;
}
export interface DataExportTable extends DataExportDocumentElement {
    columns: DataExportColumn[];
    data: Array<{
        data: Array<string | number | undefined>;
        options?: DataExportDataRowOptions;
    }>;
    tableCssClass: string;
    headerRowCssClass: string;
    dataRowCssBaseClass: string;
    totalDefinitions?: DataExportTotalRow[];
    totals?: any[][];
}
export interface DataExportTitle extends DataExportDocumentElement {
    title: string;
    cssClass: string;
}
export interface DataExportDocument {
    documentName: string;
    structure: DataExportDocumentElement[];
}
export declare function prepareDataExportTable(table: DataExportTable): DataExportTable;
export declare function prepareDataExportStructure(structure: DataExportDocumentElement[]): DataExportDocumentElement[];
//# sourceMappingURL=DataExport.d.ts.map