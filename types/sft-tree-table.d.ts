import { IDataEntry } from './model/IDataEntry';
import { TreeTable } from './model/TreeTable';
import { TreeTableCFilter } from './model/TreeTableCFilter';
import { TreeTableRFilter } from './model/TreeTableRFilter';
import { ICell } from './view/ICell';
export declare class SftTreeTableComponent<T extends IDataEntry> {
    private treeTable;
    private rightMatrix;
    private leftMatrix;
    private totalsColumn;
    constructor(treeTable: TreeTable<T>);
    ngAfterViewInit(): void;
    columnClick(element: ICell<T>): void;
    rowClick(element: ICell<T>): void;
    toggleChildren(columnNode: TreeTableCFilter<T>): void;
    toggleRChildren(rowNode: TreeTableRFilter<T>): void;
    /**
     * fornisce una colonna in ogni cella indica se la riga corrispondente è una riga di totali o no
     *
     * @param {TreeTableRFilter<T>[]} rowsLeaves
     * @returns {boolean[]}
     * @memberof SftTreeTableComponent
     */
    generateTotalsRowFlags(rowsLeaves: Array<TreeTableRFilter<T>>): boolean[];
    /**
     * Genera la matrice di colonne della zona a sinistra della tabella, ovvero quella contenente le definizioni delle
     * intestazioni delle righe
     *
     * @param {TreeTableRFilter<T>[]} rowsTree
     * @param {TreeTableRFilter<T>[]} rowsLeaves
     * @memberof SftTreeTableComponent
     */
    generateRowHeadersMatrix(rowsTree: Array<TreeTableRFilter<T>>, rowsLeaves: Array<TreeTableRFilter<T>>, maxLevel: number, columnsMaxLevel: number): Array<Array<ICell<T>>>;
    /**
     * Aggiunge il branch del filtro di riga all'interno della matrice delle celle
     *
     * @param {TreeTableRFilter<T>} rowFilter
     * @param {ICell[][]} [matrix]
     * @memberof SftTreeTableComponent
     */
    insertRowBranch(filter: TreeTableRFilter<T>, matrix: Array<Array<ICell<T>>>): void;
    /**
     * Genera la matrice della zona destra della tabella, contenente la intestazione e le righe di dati
     * (attenzione che quella che fornisce è una matrice di colonne, non di righe, questo perchè la UI si basa
     * sulla visualizzazione di colonne)
     *
     * @param {TreeTableCFilter<T>[]} columnsTree
     * @param {TreeTableCFilter<T>[]} columnsLeaves
     * @param {((string | number)[][])} data
     * @memberof SftTreeTableComponent
     */
    generateHeaderAndDataMatrix(columnsTree: Array<TreeTableCFilter<T>>, columnsLeaves: Array<TreeTableCFilter<T>>, data: Array<Array<string | number>>, totalsColumn: boolean[]): Array<Array<ICell<T>>>;
    /**
     * Genera una matrice rettangolare di oggetti che rispecchia la struttura visiva della intestazione della tabella
     * (non contiene i dati)
     *
     * @param {TreeTableCFilter<T>[]} columnsTree
     * @param {TreeTableCFilter<T>[]} columnsLeaves
     * @memberof SftTreeTableComponent
     */
    generateHeaderMatrix(columnsTree: Array<TreeTableCFilter<T>>, columnsLeaves: Array<TreeTableCFilter<T>>): Array<Array<ICell<T>>>;
    /**
     * Genera un array bidimensionale in cui la prima coordinata è il livello nell'albero
     *
     * @param {TreeTableCFilter<T>[]} columnsTree
     * @param {TreeTableCFilter<T>[][]} currentMatrix
     * @memberof SftTreeTableComponent
     */
    generateColumnsLevelsMatrix(columnsTree: Array<TreeTableCFilter<T>>, currentMatrix: Array<Array<TreeTableCFilter<T>>>): void;
}
//# sourceMappingURL=sft-tree-table.d.ts.map