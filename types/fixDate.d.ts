/// <reference types="node" />
export declare function fixDate(date: string | number | Date | Buffer | undefined | null, nullValue?: Date): Date | undefined;
//# sourceMappingURL=fixDate.d.ts.map