import { DataExportColumn } from './DataExport';
export declare function setDataExportFormatter(column: DataExportColumn): DataExportColumn;
export declare function getDataExportFormatter(type: string): (value: any) => string | number | null | undefined;
//# sourceMappingURL=DataExportFormatFunctions.d.ts.map