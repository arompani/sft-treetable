import { IDataEntry } from '../model/IDataEntry';
import { TreeTableCFilter } from '../model/TreeTableCFilter';
import { TreeTableRFilter } from '../model/TreeTableRFilter';
export interface ICell<T extends IDataEntry> {
    placeholder: boolean;
    height: number;
    width: number;
    text?: string | number;
    spacer?: boolean;
    topleft?: boolean;
    columnFilter?: TreeTableCFilter<T>;
    rowFilter?: TreeTableRFilter<T>;
    isTotal?: boolean;
    expanding?: boolean;
}
//# sourceMappingURL=ICell.d.ts.map