import { Subject } from 'rxjs';
export class SftTreeTableComponent {
    constructor(treeTable) {
        this.rightMatrix = new Subject();
        this.leftMatrix = new Subject();
        this.totalsColumn = [];
        this.treeTable = treeTable;
    }
    ngAfterViewInit() {
        const onNewValue = (value) => {
            setTimeout(() => {
                if (this.treeTable.rowAttributes !== undefined && this.treeTable.columnAttributes !== undefined) {
                    this.totalsColumn = this.generateTotalsRowFlags(this.treeTable.rowsLeaves);
                    this.leftMatrix.next(this.generateRowHeadersMatrix(this.treeTable.rowsTree, this.treeTable.rowsLeaves, this.treeTable.rowAttributes.maxSubLevel, this.treeTable.columnAttributes.maxSubLevel));
                    this.rightMatrix.next(this.generateHeaderAndDataMatrix(this.treeTable.columnsTree, this.treeTable.columnsLeaves, value, this.totalsColumn));
                }
            }, 0);
        };
        this.treeTable.filteredData$.subscribe(onNewValue.bind(this));
    }
    columnClick(element) {
        if (element.columnFilter && !element.placeholder) {
            if (element.columnFilter.hasSubColumns) {
                element.expanding = true;
                element.columnFilter.hideChildren = !element.columnFilter.hideChildren;
                this.treeTable.updateDataAfterColumnsChange();
            }
        }
    }
    rowClick(element) {
        if (element.rowFilter) {
            if (element.rowFilter.hasSubRows) {
                element.rowFilter.hideChildren = !element.rowFilter.hideChildren;
                this.treeTable.updateData();
            }
        }
    }
    toggleChildren(columnNode) {
        columnNode.hideChildren = !columnNode.hideChildren;
        this.treeTable.updateData();
    }
    toggleRChildren(rowNode) {
        rowNode.hideChildren = !rowNode.hideChildren;
        this.treeTable.updateData();
    }
    /**
     * fornisce una colonna in ogni cella indica se la riga corrispondente è una riga di totali o no
     *
     * @param {TreeTableRFilter<T>[]} rowsLeaves
     * @returns {boolean[]}
     * @memberof SftTreeTableComponent
     */
    generateTotalsRowFlags(rowsLeaves) {
        return rowsLeaves.map((rl) => (rl.isTotalRow ? true : false));
    }
    /**
     * Genera la matrice di colonne della zona a sinistra della tabella, ovvero quella contenente le definizioni delle
     * intestazioni delle righe
     *
     * @param {TreeTableRFilter<T>[]} rowsTree
     * @param {TreeTableRFilter<T>[]} rowsLeaves
     * @memberof SftTreeTableComponent
     */
    generateRowHeadersMatrix(rowsTree, rowsLeaves, maxLevel, columnsMaxLevel) {
        // il numero di colonne è pari al massimo livello trovato per i filtri
        const matrix = [];
        for (let i = 0; i < maxLevel + 1; i++) {
            matrix.push([]);
        }
        for (let i = 0; i < maxLevel + 1; i++) {
            for (let g = 0; g < columnsMaxLevel + 1; g++) {
                matrix[i].push({
                    width: 1,
                    height: 1,
                    placeholder: true,
                    text: '',
                    topleft: true,
                });
            }
        }
        // è possibile che capiti una foglia non al massimo livello, questo significa che
        // devono essere creati dei placeholder sia per larghezze >1 che per altezze >1
        // imposto la matrice delle intestazioni di riga tramite un depth first sull'albero
        //  delle intestazioni
        rowsTree.forEach((filter) => {
            this.insertRowBranch(filter, matrix);
        });
        return matrix;
    }
    /**
     * Aggiunge il branch del filtro di riga all'interno della matrice delle celle
     *
     * @param {TreeTableRFilter<T>} rowFilter
     * @param {ICell[][]} [matrix]
     * @memberof SftTreeTableComponent
     */
    insertRowBranch(filter, matrix) {
        const height = filter.isLeaf ? 1 : filter.subLeaves || 0;
        const width = filter.isLeaf ? (filter.maxLevel || 0) - (filter.level || 1) + 1 : 1;
        for (let g = 0; g < width; g++) {
            for (let i = 0; i < height; i++) {
                matrix[(filter.level || 0) + g].push({
                    height,
                    width,
                    placeholder: i !== 0 || g !== 0,
                    text: filter.text,
                    spacer: g === 0 && i !== 0,
                    rowFilter: filter,
                    isTotal: filter.isTotalRow,
                });
            }
        }
        filter.subRows.forEach((subFilter) => {
            this.insertRowBranch(subFilter, matrix);
        });
    }
    /**
     * Genera la matrice della zona destra della tabella, contenente la intestazione e le righe di dati
     * (attenzione che quella che fornisce è una matrice di colonne, non di righe, questo perchè la UI si basa
     * sulla visualizzazione di colonne)
     *
     * @param {TreeTableCFilter<T>[]} columnsTree
     * @param {TreeTableCFilter<T>[]} columnsLeaves
     * @param {((string | number)[][])} data
     * @memberof SftTreeTableComponent
     */
    generateHeaderAndDataMatrix(columnsTree, columnsLeaves, data, totalsColumn) {
        const headerMatrix = this.generateHeaderMatrix(columnsTree, columnsLeaves);
        for (let i = 0; i < columnsLeaves.length; i++) {
            // recupero la colonna dai dati filtrati
            const dataColumn = data.map((r) => r[i]); // TODO migliorare mettendo tutto in una sola chiamata
            // per ogni dato della colonna lo aggiungo alla colonna sotto gli elementi della intestazione
            dataColumn.forEach((dc, index) => {
                headerMatrix[i].push({
                    height: 1,
                    width: 1,
                    placeholder: false,
                    text: dc,
                    isTotal: totalsColumn[index],
                });
            });
        }
        return headerMatrix;
    }
    /**
     * Genera una matrice rettangolare di oggetti che rispecchia la struttura visiva della intestazione della tabella
     * (non contiene i dati)
     *
     * @param {TreeTableCFilter<T>[]} columnsTree
     * @param {TreeTableCFilter<T>[]} columnsLeaves
     * @memberof SftTreeTableComponent
     */
    generateHeaderMatrix(columnsTree, columnsLeaves) {
        const levels = [];
        let matrix;
        this.generateColumnsLevelsMatrix(columnsTree, levels);
        // con la matrice contenente ogni foglia per livello occorre creare la matrice finale della intestazione.
        // la matrice dei livelli va convertita in una visione a colonne (ora è a righe).
        // per ogni livello bisogna considerare, per la dimensione orizzontale il numero di foglie che il filtro
        // ha al termine del suo ramo, mentre verticalmente l'altezza è decisa, se il filtro è una foglia,
        // dal suo valore di livello rispetto al livello massimo presente nell'albero, altrimenti ha altezza unitaria
        // inizializzazione della matrice con il numero di colonne necessarie
        matrix = columnsLeaves.map((g) => []);
        const lockedColumns = [];
        // aggiunta celle della matrice
        levels.forEach((levelObjects) => {
            // per ogni livello inserisco gli elementi della riga
            let currentColumn = 0;
            // trovo la prima colonna non bloccata
            for (let i = 0; i < matrix.length; i++) {
                if (lockedColumns.find((val) => val === i) === undefined) {
                    currentColumn = i;
                    break;
                }
            }
            levelObjects.forEach((filter) => {
                // ogni elemento controllo se è una foglia, e, se necessario ne calcolo il numero di
                // colonne che occuperà
                const height = filter.isLeaf ? (filter.maxLevel || 0) - (filter.level || 0) + 1 : 1;
                if (height > 1) {
                    // se l'altezza è maggiore di 1 allora blocco la colonna da qualsiasi inserimento
                    lockedColumns.push(currentColumn);
                }
                // inserisco la cella corrispondente
                // la larghezza è data dal numero di foglie presenti nel sottoramo del filtro stesso
                matrix[currentColumn].push({
                    placeholder: false,
                    height,
                    width: filter.subLeaves || 0,
                    text: filter.text,
                    columnFilter: filter,
                });
                // per semplificare il posizionamento e mantenere la struttura a celle è necessario inserire
                // dei placeholder se la altezza della colonna è maggiore di 1 (questi placeholdere non verranno
                // poi visualizzati nella tabella finale)
                for (let i = 1; i < height; i++) {
                    matrix[currentColumn].push({
                        placeholder: true,
                        height: 1,
                        width: 1,
                        text: filter.text,
                        spacer: true,
                        columnFilter: filter,
                    });
                }
                // ho inserito il testo nella cella corretta che però deve aggiungere se stesso anche nelle colonne
                //  successive se non è una foglia per mantenere la struttura visiva ad albero
                // trovo la prima colonna non bloccata
                for (let i = currentColumn + 1; i < matrix.length; i++) {
                    if (lockedColumns.find((val) => val === i) === undefined) {
                        currentColumn = i;
                        break;
                    }
                }
                for (let i = 1; i < (filter.subLeaves || 0); i++) {
                    // procede ad aggiungere celle alle colonne successive fino a che non ne ha aggiunti un numero
                    // pari alla quantità di foglie che il suo sottoramo contiene
                    matrix[currentColumn].push({
                        placeholder: true,
                        height,
                        width: 1,
                        text: filter.text,
                        columnFilter: filter,
                    });
                    currentColumn++;
                }
            });
        });
        return matrix;
    }
    /**
     * Genera un array bidimensionale in cui la prima coordinata è il livello nell'albero
     *
     * @param {TreeTableCFilter<T>[]} columnsTree
     * @param {TreeTableCFilter<T>[][]} currentMatrix
     * @memberof SftTreeTableComponent
     */
    generateColumnsLevelsMatrix(columnsTree, currentMatrix) {
        columnsTree.forEach((filter) => {
            if ((filter.level || 0) >= currentMatrix.length) {
                currentMatrix.push([filter]);
            }
            else {
                currentMatrix[filter.level || 0].push(filter);
            }
            if (filter.subColumns.length > 0) {
                this.generateColumnsLevelsMatrix(filter.subColumns, currentMatrix);
            }
        });
    }
}
//# sourceMappingURL=sft-tree-table.js.map