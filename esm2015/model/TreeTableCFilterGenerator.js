export class ConstantTreeTableCFilterGenerator {
    constructor(filters) {
        this.filters = filters;
        this.isDynamic = false;
    }
    getFilters(data, parent) {
        const newFilters = this.filters.map((f) => f.clone(data, parent));
        newFilters.forEach((f) => {
            f.parent = parent;
        }); // update parents on filters
        return newFilters;
    }
    clone(data, parent) {
        return new ConstantTreeTableCFilterGenerator(this.filters.map((f) => f.clone(data, parent)));
    }
}
//# sourceMappingURL=TreeTableCFilterGenerator.js.map