// tslint:disable:no-unsafe-any
export function generateSortDescending(columnName) {
    return (a, b) => {
        if (a[columnName] < b[columnName]) {
            return 1;
        }
        if (a[columnName] > b[columnName]) {
            return -1;
        }
        return 0;
    };
}
export function generateSortAscending(columnName) {
    return (a, b) => {
        if (a[columnName] < b[columnName]) {
            return -1;
        }
        if (a[columnName] > b[columnName]) {
            return 1;
        }
        return 0;
    };
}
export function stringSortAscending(a, b) {
    if (a < b) {
        return -1;
    }
    if (a > b) {
        return 1;
    }
    return 0;
}
export function stringSortDescending(a, b) {
    if (a < b) {
        return 1;
    }
    if (a > b) {
        return -1;
    }
    return 0;
}
//# sourceMappingURL=Sorting.js.map