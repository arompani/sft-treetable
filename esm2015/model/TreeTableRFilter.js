/**
 * Definizione di una riga
 *
 * @export
 * @class TreeTableRFilter
 * @template T
 */
export class TreeTableRFilter {
    constructor(options, data) {
        this.options = options;
        this.currentSubRows = this.options.subGenerator ? this.options.subGenerator.getFilters(data, this) : [];
        this.childrenHidden = this.options.hideChildren;
    }
    get text() {
        return this.options.text;
    }
    get subRows() {
        return [
            ...this.currentSubRows.filter((f) => f.options.forceShow),
            ...(!this.hideChildren ? this.currentSubRows.filter((f) => !f.options.forceShow) : []),
        ];
    }
    get parent() {
        return this.options.parent;
    }
    set parent(newParent) {
        this.options.parent = newParent;
    }
    get hideChildren() {
        return this.childrenHidden;
    }
    set hideChildren(newValue) {
        this.childrenHidden = newValue;
    }
    /**
     * true se il filtro ha sottocolonne (senza contare quelle dovute al collapsed generator)
     *
     * @readonly
     * @type {boolean}
     * @memberof TreeTableRFilter
     */
    get hasSubRows() {
        return this.currentSubRows.length > 0;
    }
    get isLeaf() {
        return this.subRows && this.subRows.length === 0;
    }
    get isTotalRow() {
        return this.options.isTotalRow;
    }
    get filterFunction() {
        return this.options.filterFunction;
    }
    getLeaves() {
        const activeFilters = this.subRows;
        const leaves = [];
        if (this.currentSubRows.length === 0 || activeFilters.length === 0) {
            leaves.push(this);
        }
        else {
            activeFilters.forEach((af) => {
                af.getLeaves().forEach((l) => {
                    leaves.push(l);
                });
            });
        }
        return leaves;
    }
    getParsedData(data) {
        return this.applyTopFilters(data).filter(this.options.filterFunction);
    }
    applyTopFilters(data) {
        let filters = [this];
        let currentNode = this;
        let filteredData = data;
        while (currentNode.parent !== undefined) {
            currentNode = currentNode.parent;
            filters = [currentNode, ...filters];
        }
        // tslint:disable-next-line:prefer-for-of
        for (let i = 0; i < filters.length; i++) {
            filteredData = filteredData.filter(filters[i].options.filterFunction);
        }
        return filteredData;
    }
    calculateAttributes(currentLevel) {
        this.level = currentLevel;
        const activeFilters = this.subRows;
        let subLeaves = 0;
        let maxSubLevel = currentLevel;
        if (this.subRows.length === 0 || activeFilters.length === 0) {
            this.subLeaves = 0;
            subLeaves = 1;
        }
        else {
            this.subRows.forEach((sc) => {
                const result = sc.calculateAttributes(currentLevel + 1);
                maxSubLevel = maxSubLevel < result.maxSubLevel ? result.maxSubLevel : maxSubLevel;
                subLeaves += result.subLeaves;
            });
            this.subLeaves = subLeaves;
        }
        this.maxSubLevel = maxSubLevel;
        return { subLeaves, maxSubLevel };
    }
    propagateTopDownAttributes(branchSubLeaves, maxLevel) {
        const activeFilters = this.subRows;
        this.maxLevel = maxLevel;
        this.branchSubLeaves = branchSubLeaves;
        if (!(this.subRows.length === 0 || activeFilters.length === 0)) {
            this.subRows.forEach((sc) => {
                sc.propagateTopDownAttributes(branchSubLeaves, maxLevel);
            });
        }
    }
}
//# sourceMappingURL=TreeTableRFilter.js.map