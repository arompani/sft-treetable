export class MergeTreeTableCFilterGenerator {
    constructor(filters) {
        this.filters = filters;
        this.isDynamic = false;
    }
    getFilters(data, parent) {
        const newFilters = [].concat.apply([], this.filters.map((f) => f.getFilters(data, parent)));
        newFilters.forEach((f) => {
            f.parent = parent;
        }); // update parents on filters
        return newFilters;
    }
    clone(data, parent) {
        return new MergeTreeTableCFilterGenerator([].concat
            .apply([], this.filters)
            .map((f) => f.clone(data, parent)));
    }
}
//# sourceMappingURL=MergeTreeTableCFilterGenerator.js.map