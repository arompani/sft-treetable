import { stringSortAscending, stringSortDescending } from '../Sorting';
import { TreeTableRFilter } from '../TreeTableRFilter';
export class DistinctTreeTableRFilterGenerator {
    constructor(options) {
        this.options = options;
        this.isDynamic = false;
    }
    getFilters(data, parent) {
        const names = [];
        data.forEach((d) => {
            if (names.find((n) => n === d[this.options.columnName]) === undefined) {
                names.push(d[this.options.columnName]);
            }
        });
        if (this.options.sort) {
            if (this.options.sortFunction) {
                names.sort(this.options.sortFunction);
            }
            else {
                names.sort(this.options.sortDescending ? stringSortDescending : stringSortAscending);
            }
        }
        const ret = names.map((n) => new TreeTableRFilter({
            subGenerator: this.options.subGenerator,
            text: this.options.translatorFunction ? this.options.translatorFunction(n) : n,
            hideChildren: this.options.hideChildren,
            forceShow: this.options.forceShow,
            filterFunction: (row) => row[this.options.columnName] === n,
            parent,
        }, data.filter((row) => row[this.options.columnName] === n)));
        if (!this.options.hideTotals) {
            ret.push(new TreeTableRFilter({
                subGenerator: undefined,
                text: `Totale ${parent ? ` ${parent.text}` : ''}`,
                hideChildren: this.options.hideChildren,
                forceShow: this.options.forceShow,
                filterFunction: parent ? parent.filterFunction : (row) => true,
                parent,
                isTotalRow: true,
            }, data.filter(parent ? parent.filterFunction : (row) => true)));
        }
        return ret;
    }
}
//# sourceMappingURL=DistinctTreeTableRFilterGenerator.js.map