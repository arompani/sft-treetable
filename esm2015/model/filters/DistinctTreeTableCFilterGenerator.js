import { stringSortAscending, stringSortDescending } from '../Sorting';
import { TreeTableCFilter } from '../TreeTableCFilter';
export class DistinctTreeTableCFilterGenerator {
    constructor(options) {
        this.options = options;
        this.isDynamic = false;
    }
    getFilters(data, parent) {
        const names = [];
        data.forEach((d) => {
            if (names.find((n) => n === d[this.options.columnName]) === undefined) {
                names.push(d[this.options.columnName]);
            }
        });
        if (this.options.sort) {
            if (this.options.sortFunction) {
                names.sort(this.options.sortFunction);
            }
            else {
                names.sort(this.options.sortDescending ? stringSortDescending : stringSortAscending);
            }
        }
        return names.map((n) => new TreeTableCFilter({
            text: this.options.translatorFunction ? this.options.translatorFunction(n) : n,
            subColumnsGenerator: this.options.subColumnsGenerator,
            collapsedSubColumnsGenerator: this.options.collapsedSubColumnsGenerator,
            hideChildren: this.options.hideChildren,
            forceShow: this.options.forceShow,
            reduceFunction: this.options.reduceFunction,
            filterFunction: (row) => row[this.options.columnName] === n,
            parent,
        }, data.filter((column) => column[this.options.columnName] === n)));
    }
    clone(data, parent) {
        return new DistinctTreeTableCFilterGenerator({
            columnName: this.options.columnName,
            subColumnsGenerator: this.options.subColumnsGenerator
                ? this.options.subColumnsGenerator.clone(data, parent)
                : undefined,
            collapsedSubColumnsGenerator: this.options.collapsedSubColumnsGenerator
                ? this.options.collapsedSubColumnsGenerator.clone(data, parent)
                : undefined,
            hideChildren: this.options.hideChildren,
            forceShow: this.options.forceShow,
            translatorFunction: this.options.translatorFunction,
            reduceFunction: this.options.reduceFunction,
        });
    }
}
//# sourceMappingURL=DistinctTreeTableCFilterGenerator.js.map