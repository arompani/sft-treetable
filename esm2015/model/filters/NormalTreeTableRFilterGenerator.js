import { TreeTableRFilter } from '../TreeTableRFilter';
export class NormalTreeTableRFilterGenerator {
    constructor(options) {
        this.options = options;
        this.isDynamic = false;
    }
    getFilters(data, parent) {
        const ret = data.map((d) => new TreeTableRFilter({
            subGenerator: this.options.subGenerator,
            text: d[this.options.textBinding],
            hideChildren: true,
            forceShow: false,
            filterFunction: (row) => row === d,
            parent,
        }, data));
        return ret;
    }
}
//# sourceMappingURL=NormalTreeTableRFilterGenerator.js.map