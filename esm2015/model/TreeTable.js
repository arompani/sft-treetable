import { BehaviorSubject } from 'rxjs';
/**
 * Gestione delle righe e colonne della tabella e filtraggio
 *
 * @export
 * @class TreeTable
 * @template T
 */
export class TreeTable {
    constructor(rowsFilterGenerator, columnsFilterGenerator, dataTransformer, reduceFunction) {
        this.rowsFilterGenerator = rowsFilterGenerator;
        this.columnsFilterGenerator = columnsFilterGenerator;
        this.dataTransformer = dataTransformer;
        this.reduceFunction = reduceFunction;
        this.rowAttributes = undefined;
        this.columnAttributes = undefined;
        this.columns = [];
        this._columnsLeaves = [];
        this.rows = [];
        this._rowsLeaves = [];
        this.data = [];
        this.fData = [];
        this.filterGeneratorsChanged = false;
        this.rowsFilteredData = [];
        this.fData$ = new BehaviorSubject([]);
    }
    /**
     * cambia gli attuali generatori di filtri delle righe
     *
     * @memberof TreeTable
     */
    set rowFiltersGenerator(generator) {
        this.rowsFilterGenerator = generator;
        this.filterGeneratorsChanged = true;
    }
    /**
     * cambia gli attuali generatori di filtri delle colonne
     *
     * @memberof TreeTable
     */
    set columnFiltersGenerator(generator) {
        this.columnsFilterGenerator = generator;
        this.filterGeneratorsChanged = true;
    }
    /**
     * cambia la funzione di riduzione applicata
     *
     * @memberof TreeTable
     */
    set currentReduceFunction(reduceFunction) {
        this.reduceFunction = reduceFunction;
    }
    get filteredData$() {
        if (this.fData === undefined) {
            this.updateData();
        }
        return this.fData$; // apply filters
    }
    get filteredData() {
        if (this.fData === undefined) {
            this.updateData();
        }
        return this.fData;
    }
    /**
     * fornisce l'albero completo delle colonne
     *
     * @readonly
     * @type {TreeTableCFilter<T>}
     * @memberof TreeTable
     */
    get columnsTree() {
        return this.columns;
    }
    /**
     * cambia l'albero delle colonne con quello indicato ed avvia la rigenerazione della
     * tabella dei dati filtrati
     *
     * @memberof TreeTable
     */
    set columnsTree(newTree) {
        this.columns = newTree;
        this.updateData();
    }
    get columnsLeaves() {
        return this._columnsLeaves;
    }
    /**
     * fornisce l'albero completo delle righe
     *
     * @readonly
     * @type {TreeTableRFilter<T>}
     * @memberof TreeTable
     */
    get rowsTree() {
        return this.rows;
    }
    set rowsTree(newTree) {
        this.rows = newTree;
        this.updateData();
    }
    get rowsLeaves() {
        return this._rowsLeaves;
    }
    updateDataAfterColumnsChange() {
        this.fData = [];
        this._columnsLeaves = [].concat(...this.columns.map((c) => c.getLeaves()));
        this.columnAttributes = this.calculateCTreeAttributes(-1);
        this.rowsFilteredData.forEach((rowData) => {
            const columnLeaves = [].concat(...this.columns.map((c) => c.getLeaves()));
            const newRow = [];
            columnLeaves.forEach((cl) => {
                const filteredRows = cl.applyFiltersToData(rowData);
                const reducedRow = this.reduceFunction(filteredRows);
                newRow.push(cl.getValue(reducedRow));
            });
            if (!this.fData) {
                this.fData = [];
            }
            this.fData.push(newRow);
        });
        this.fData$.next(this.fData);
    }
    /**
     * aggiorna i dati filtrando la tabella fornita precedentemente
     * o, se forniti, applica il filtro sui dati forniti come parametro
     * e li utilizza per le prossime chiamate
     *
     * @memberof TreeTable
     */
    updateData(data, forceFiltersUpdate = false) {
        if (data !== undefined) {
            this.data = this.dataTransformer(data); // se data è fornito come parametro allora imposta quello per la tabella corrente
        }
        if (data !== undefined || forceFiltersUpdate || this.filterGeneratorsChanged) {
            this.columns = this.columnsFilterGenerator.getFilters(this.data);
            this._columnsLeaves = [].concat(...this.columns.map((c) => c.getLeaves()));
            this.rows = this.rowsFilterGenerator.getFilters(this.data);
            this._rowsLeaves = [].concat(...this.rows.map((c) => c.getLeaves()));
            this.filterGeneratorsChanged = false;
        }
        if (this.data.length === 0) {
            this.fData = [];
            this.columns = [];
            this.rows = [];
            this._rowsLeaves = [];
            this._columnsLeaves = [];
            this.fData$.next(this.fData);
            return;
        }
        this.rowAttributes = this.calculateRTreeAttributes(-1);
        this.columnAttributes = this.calculateCTreeAttributes(-1);
        const rowLeaves = [].concat(...this.rows.map((c) => c.getLeaves()));
        this._rowsLeaves = rowLeaves;
        const step1 = rowLeaves.map((rl) => {
            return { rowData: rl.getParsedData(this.data), isTotalRow: rl.isTotalRow };
        });
        this.fData = [];
        this.rowsFilteredData = step1;
        step1.forEach((rowData) => {
            const columnLeaves = [].concat(...this.columns.map((c) => c.getLeaves()));
            const newRow = [];
            columnLeaves.forEach((cl) => {
                const filteredRows = cl.applyFiltersToData(rowData);
                const reducedRow = this.reduceFunction(filteredRows);
                newRow.push(cl.getValue(reducedRow));
            });
            if (!this.fData) {
                this.fData = [];
            }
            this.fData.push(newRow);
        });
        this.fData$.next(this.fData);
    }
    calculateRTreeAttributes(currentLevel) {
        let subLeaves = 0;
        let maxLevel = currentLevel;
        if (this.rows.length === 0) {
            subLeaves = 1;
        }
        else {
            this.rows.forEach((sr) => {
                const result = sr.calculateAttributes(currentLevel + 1);
                maxLevel = maxLevel < result.maxSubLevel ? result.maxSubLevel : maxLevel;
                subLeaves += result.subLeaves;
            });
            this.rows.forEach((sr) => {
                sr.propagateTopDownAttributes(sr.subLeaves || 0, maxLevel);
            });
        }
        return { subLeaves, maxSubLevel: maxLevel };
    }
    calculateCTreeAttributes(currentLevel) {
        let subLeaves = 0;
        let maxLevel = currentLevel;
        if (this.columns.length === 0) {
            subLeaves = 1;
        }
        else {
            this.columns.forEach((sc) => {
                const result = sc.calculateAttributes(currentLevel + 1);
                maxLevel = maxLevel < result.maxSubLevel ? result.maxSubLevel : maxLevel;
                subLeaves += result.subLeaves;
            });
            this.columns.forEach((sc) => {
                sc.propagateTopDownAttributes(sc.subLeaves || 0, maxLevel);
            });
        }
        return { subLeaves, maxSubLevel: maxLevel };
    }
}
//# sourceMappingURL=TreeTable.js.map