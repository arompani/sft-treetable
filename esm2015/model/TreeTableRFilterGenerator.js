export class ConstantTreeTableRFilterGenerator {
    constructor(filters) {
        this.filters = filters;
        this.isDynamic = false;
    }
    getFilters(data) {
        return [this.filters];
    }
}
//# sourceMappingURL=TreeTableRFilterGenerator.js.map