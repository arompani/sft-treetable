/**
 * Definizione di una colonna
 *
 * @export
 * @class TreeTableCFilter
 * @template T
 */
export class TreeTableCFilter {
    constructor(options, data) {
        this.options = options;
        this.expanding = false;
        this.currentSubColumns = this.options.subColumnsGenerator
            ? this.options.subColumnsGenerator.getFilters(data, this)
            : [];
        this.currentCollapsedSubColumns = this.options.collapsedSubColumnsGenerator
            ? this.options.collapsedSubColumnsGenerator.getFilters(data, this)
            : [];
        this.childrenHidden = this.options.hideChildren;
    }
    get subColumns() {
        return [
            ...this.currentSubColumns.filter((f) => f.options.forceShow),
            ...(!this.hideChildren ? this.currentSubColumns.filter((f) => !f.options.forceShow) : []),
        ];
    }
    get text() {
        return this.options.text;
    }
    get parent() {
        return this.options.parent;
    }
    set parent(parent) {
        this.options.parent = parent;
    }
    get hideChildren() {
        return this.childrenHidden;
    }
    set hideChildren(newValue) {
        this.childrenHidden = newValue;
    }
    get isLeaf() {
        return this.subColumns && this.subColumns.length === 0;
    }
    get hasSubColumns() {
        return this.currentSubColumns.length > 0;
    }
    getLeaves() {
        const activeFilters = this.subColumns;
        const leaves = [];
        if (activeFilters.length === 0) {
            leaves.push(this);
        }
        else {
            activeFilters.forEach((af) => {
                af.getLeaves().forEach((l) => {
                    leaves.push(l);
                });
            });
        }
        return leaves;
    }
    getValue(row) {
        let baseValue = this.options.reduceFunction(row);
        if (this.options.translatorFunction) {
            baseValue = this.options.translatorFunction(baseValue);
        }
        return baseValue;
    }
    clone(data, parent) {
        return new TreeTableCFilter({
            text: this.options.text,
            subColumnsGenerator: this.options.subColumnsGenerator
                ? this.options.subColumnsGenerator.clone(data, parent)
                : undefined,
            collapsedSubColumnsGenerator: this.options.collapsedSubColumnsGenerator
                ? this.options.collapsedSubColumnsGenerator.clone(data, parent)
                : undefined,
            hideChildren: this.options.hideChildren,
            forceShow: this.options.forceShow,
            filterFunction: this.options.filterFunction,
            reduceFunction: this.options.reduceFunction,
            translatorFunction: this.options.translatorFunction,
            parent,
        }, data);
    }
    applyFiltersToData(data) {
        let filters = [this];
        let currentNode = this;
        while (currentNode.parent !== undefined) {
            currentNode = currentNode.parent;
            filters = [currentNode, ...filters];
        }
        let fData = data.rowData;
        // tslint:disable-next-line:prefer-for-of
        for (let i = 0; i < filters.length; i++) {
            if (filters[i].options.filterFunction !== undefined) {
                fData = fData.filter(filters[i].options.filterFunction);
            }
        }
        return { rowData: fData, isTotalRow: data.isTotalRow };
    }
    applyTopFilters(row) {
        let filters = [this];
        let currentNode = this;
        while (currentNode.parent !== undefined) {
            currentNode = currentNode.parent;
            filters = [currentNode, ...filters];
        }
        let validRow = true;
        // tslint:disable-next-line:prefer-for-of
        for (let i = 0; i < filters.length; i++) {
            if (filters[i].options.filterFunction !== undefined) {
                validRow = filters[i].options.filterFunction(row);
            }
        }
        return validRow ? row : undefined;
    }
    calculateAttributes(currentLevel) {
        this.level = currentLevel;
        const activeFilters = this.subColumns;
        let subLeaves = 0;
        let maxSubLevel = currentLevel;
        if (this.subColumns.length === 0 || activeFilters.length === 0) {
            this.subLeaves = 0;
            subLeaves = 1;
        }
        else {
            this.subColumns.forEach((sc) => {
                const result = sc.calculateAttributes(currentLevel + 1);
                maxSubLevel = maxSubLevel < result.maxSubLevel ? result.maxSubLevel : maxSubLevel;
                subLeaves += result.subLeaves;
            });
            this.subLeaves = subLeaves;
        }
        this.maxSubLevel = maxSubLevel;
        return { subLeaves, maxSubLevel };
    }
    propagateTopDownAttributes(branchSubLeaves, maxLevel) {
        const activeFilters = this.subColumns;
        this.maxLevel = maxLevel;
        this.branchSubLeaves = branchSubLeaves;
        // tslint:disable-next-line:no-empty
        if (activeFilters.length === 0) {
        }
        else {
            activeFilters.forEach((sc) => {
                sc.propagateTopDownAttributes(branchSubLeaves, maxLevel);
            });
        }
    }
}
//# sourceMappingURL=TreeTableCFilter.js.map