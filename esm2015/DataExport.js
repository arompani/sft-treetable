import { getDataExportFormatter, setDataExportFormatter } from './DataExportFormatFunctions';
export function prepareDataExportTable(table) {
    table.columns = table.columns.map((col) => setDataExportFormatter(col));
    const baseData = table.data;
    table.data = table.data.map((row) => {
        return {
            data: row.data.map((cell, colIndex) => {
                const formatFunction = table.columns[colIndex].formatFunction;
                if (table.columns[colIndex].hideIfNull && (cell === '' || cell === '0' || cell === 0)) {
                    return '';
                }
                return formatFunction ? formatFunction(cell) : cell;
            }),
            options: row.options,
        };
    });
    if (table.totalDefinitions && table.data.length > 0) {
        const totals = table.totalDefinitions.map((td) => {
            let row = new Array(table.columns.length);
            row.fill('');
            td.columns
                .map((ci) => getDataExportFormatter(ci.format)(baseData.map((crow) => crow.data[ci.index]).reduce(ci.reduceFunction)))
                .map((val, index) => {
                return { column: td.columns[index], total: val };
            })
                .forEach((col) => {
                row[col.column.index] = col.total;
            });
            td.textInsertFunctions.forEach((tif) => {
                row = tif(row);
            });
            return row;
        });
        table.totals = totals;
    }
    return table;
}
export function prepareDataExportStructure(structure) {
    return structure.map((str) => {
        if (str.type === 'table') {
            return prepareDataExportTable(str);
        }
        else {
            return str;
        }
    });
}
//# sourceMappingURL=DataExport.js.map