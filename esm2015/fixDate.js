export function fixDate(date, nullValue) {
    if (date === undefined || date === null) {
        return nullValue;
    }
    if (Buffer.isBuffer(date)) {
        try {
            const dateString = date.toString('utf8');
            return fixDate(dateString);
        }
        catch (error) {
            // todo
        }
    }
    if (typeof date === 'string') {
        if (date === '' || date === '0000-00-00') {
            return nullValue;
        }
        else if (date.indexOf('.') !== -1 && date.indexOf('Z') === -1) {
            // formato date di sqlite 2018-07-06 02:00:00.000
            const parts = date.split('.')[0].split(' ');
            const [sDate, sTime] = parts;
            const dateParts = sDate.split('-').map((p) => Number(p));
            const timeParts = sTime.split(':').map((p) => Number(p));
            // tslint:disable-next-line:no-magic-numbers
            return new Date(dateParts[0], dateParts[1] - 1, dateParts[2], timeParts[0], timeParts[1], timeParts[2]);
        }
        else {
            if (date.indexOf('.') !== -1 && date.indexOf('Z') !== -1) {
                return new Date(date);
            }
            else {
                return new Date(Number(date));
            }
        }
    }
    else {
        return new Date(Number(date));
    }
}
//# sourceMappingURL=fixDate.js.map