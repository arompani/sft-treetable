import moment from 'moment';
import { fixDate } from './fixDate';
// tslint:disable:no-any
function formatMoney(value) {
    if (value === '' || value === undefined) {
        return Number(0).toLocaleString('it-IT', { minimumFractionDigits: 2, maximumFractionDigits: 2 });
    }
    return Number(value).toLocaleString('it-IT', { minimumFractionDigits: 2, maximumFractionDigits: 2 });
}
function toUpper(value) {
    return (value || '').toLocaleUpperCase();
}
function toLower(value) {
    return (value || '').toLocaleLowerCase();
}
function formatDate(value) {
    if (value === undefined || value === '') {
        return '';
    }
    const datevalue = fixDate(value);
    return datevalue ? moment(datevalue).format('DD/MM/YYYY') : '';
}
function formatTime(value) {
    if (value === undefined || value === '') {
        return '';
    }
    const datevalue = fixDate(value);
    return datevalue ? moment(datevalue).format('HH:mm:ss') : '';
}
function formatDateTime(value) {
    if (value === undefined || value === '') {
        return '';
    }
    const datevalue = fixDate(value);
    return datevalue ? moment(datevalue).format('DD/MM/YYYY HH:mm:ss') : '';
}
function nullZero(value) {
    return value === 0 ? '' : String(value);
}
const associations = {
    upper: { function: toUpper },
    lower: { function: toLower },
    date: { function: formatDate },
    datetime: { function: formatDateTime },
    time: { function: formatTime },
    currency: { function: formatMoney, excelFormat: 'vnd.ms-excel.numberformat:#,##0.00' },
    integer: { function: (val) => val, excelFormat: 'vnd.ms-excel.numberformat:0' },
    none: { function: (val) => val, excelFormat: 'vnd.ms-excel.numberformat:@' },
    nullzero: { function: nullZero, excelFormat: 'vnd.ms-excel.numberformat:@' },
    icell: {
        function: (val) => (val.placeholder ? '' : val.text),
        excelFormat: 'vnd.ms-excel.numberformat:@',
    },
};
export function setDataExportFormatter(column) {
    column.formatFunction = associations[column.format].function;
    column.excelDataFormat = associations[column.format].excelFormat;
    return column;
}
export function getDataExportFormatter(type) {
    return associations[type].function || associations.none.function;
}
//# sourceMappingURL=DataExportFormatFunctions.js.map